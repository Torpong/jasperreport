/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.ability;

import com.vbitx.jero.api.ability.Ability;
import com.vbitx.jero.api.ability.Ability;
import com.vbitx.jero.api.ability.AbilityProvider;
import com.vbitx.jero.api.ability.AbilityProvider;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 *
 * @author sun
 */
@Component
public class UserAbilityProvider implements AbilityProvider{
    
    private List<Ability> registeredAbility = new ArrayList<>();
    
    public UserAbilityProvider() {
        registeredAbility.add(new Ability("user", "user::list", "User list", "See all users"));
        registeredAbility.add(new Ability("user", "user::detail", "User detail", "See user detail"));
        registeredAbility.add(new Ability("user", "user::edit", "Edit user", "Edit user detail"));
        registeredAbility.add(new Ability("user", "user::delete", "Delete user", "Delete user detail"));
        registeredAbility.add(new Ability("user", "user::sync", "Sync user", "Sync user with SCGL"));
    }

    @Override
    public List<Ability> list() {
        return registeredAbility;
    }

    @Override
    public String getName() {
        return "user";
    }
}
