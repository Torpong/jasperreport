/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.ability;

import com.vbitx.jero.api.ability.Ability;
import com.vbitx.jero.api.ability.AbilityProvider;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 *
 * @author gigadot
 */
@Component
public class MainAbilityProvider implements AbilityProvider {

    private List<Ability> registeredAbility = new ArrayList<>();

    public MainAbilityProvider() {
        registeredAbility.add(new Ability("main", "main::common", "Common", "Common UI"));
        registeredAbility.add(new Ability("main", "main::sidebar", "Sidebar", "Sidebar UI"));
        registeredAbility.add(new Ability("main", "main::navbar", "Top navigation bar", "Top navigation bar UI"));
    }

    @Override
    public List<Ability> list() {
        return registeredAbility;
    }

    @Override
    public String getName() {
        return "main";
    }

}
