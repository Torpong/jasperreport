package com.vbitx.jero.api.jasper;

import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by scheaman on 8/17/16.
 */
@Service
public class ReceiptDocumentService extends DocumentService {

    private static final Logger logger = LoggerFactory.getLogger(ReceiptDocumentService.class);

    private static String template = "Receipt.jrxml";

    public ReceiptDocumentService() throws JRException {
        super(template);
    }



    public ByteArrayOutputStream generatePdf(ReceiptDocumentData data) throws JRException {
        return super.generatePdf(data.parameters, data.itemData);
    }

    public static class ReceiptDocumentData {

        private Map parameters = new HashMap();
        private List<ReceiptDocumentItemData> itemData;

        public ReceiptDocumentData(String seller, String sellerAddress, String branch,
                                   String sellerTaxID, String buyer, String buyerAddress,
                                   String buyerTaxID, String documentDate, String NoNumber,
                                   String refNumber, String stringInt,
                                   List<ReceiptDocumentItemData> itemData) {
            // Set up parameters for Jasper
            parameters.put("Seller", seller);
            parameters.put("SellerAddress", sellerAddress);
            parameters.put("Branch", branch);
            parameters.put("SellerTaxID", sellerTaxID);
            parameters.put("Buyer", buyer);
            parameters.put("BuyerAddress", buyerAddress);
            parameters.put("BuyerTaxID", buyerTaxID);
            parameters.put("DocumentDate", documentDate);
            parameters.put("NoNum",NoNumber);
            parameters.put("RefNo", refNumber);
            parameters.put("StringInt", stringInt);

            // Set up datasource for Jasper
            this.itemData = itemData;
        }



        public List<ReceiptDocumentItemData> getItemData() { return itemData; }

        public void setItemData(List<ReceiptDocumentItemData> itemData) { this.itemData = itemData; }

        public Map getParameters() { return parameters; }

        public void setParameters(Map parameters) { this.parameters = parameters; }

    }

    public static class ReceiptDocumentItemData {
        private String product ;
        private Double total;


        public ReceiptDocumentItemData(String product, Double total) {
            this.setProduct(product);
            this.setTotal(total);
        }


        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }

        public String getProduct() {
            return product;
        }

        public void setProduct(String product) {
            this.product = product;
        }
    }
}
