package com.vbitx.jero.api.jasper;

import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bossy on 29/8/2559.
 */
@Service
public class ShipmentWithAmountDocumentService extends DocumentService {

    private static final Logger logger = LoggerFactory.getLogger(ShipmentWithAmountDocumentService.class);

    private static final String template = "Shipment2.jrxml";

    public ShipmentWithAmountDocumentService() throws JRException {
        super(template);
    }

    public ByteArrayOutputStream generatePdf(ShipmentWithAmountDocumentData data) throws JRException {
        return super.generatePdf(data.parameters, data.itemData);
    }

    public static class ShipmentWithAmountDocumentData {

        private Map parameters = new HashMap();
        private List<ShipmentWithAmountDocumentItemData> itemData;

        public ShipmentWithAmountDocumentData(String carrierName, String licensePlate, String remark,
                                               String hubDestinationName, String shipmentNumber,
                                              String createdDate, List<ShipmentWithAmountDocumentItemData> itemData) {
            // Set up parameters for Jasper

            parameters.put("Contacter", carrierName);
            parameters.put("Registration", licensePlate);
            parameters.put("Remark", remark);
            parameters.put("HubDis", hubDestinationName);
            parameters.put("ShipmentNo", shipmentNumber);
            parameters.put("CreateDate", createdDate);


            // Set up datasource for Jasper
            this.itemData = itemData;
        }

        public List<ShipmentWithAmountDocumentItemData> getItemData() { return itemData; }

        public void setItemData(List<ShipmentWithAmountDocumentItemData> itemData) { this.itemData = itemData; }

        public Map getParameters() { return parameters; }

        public void setParameters(Map parameters) { this.parameters = parameters; }

    }


    public static class ShipmentWithAmountDocumentItemData {
        private String DPNO ;
        private String paymed;
        private String sender;
        private String getter;
        private String remarkT;
        private Double total;



        public ShipmentWithAmountDocumentItemData(String DPNO, String paymed, String sender, String getter, String remarkT, Double total) {
            this.setDPNO(DPNO);
            this.setPaymed(paymed);
            this.setSender(sender);
            this.setGetter(getter);
            this.setRemarkT(remarkT);
            this.setTotal(total);
        }

        public String getSender() {
            return sender;
        }

        public void setSender(String sender) {
            this.sender = sender;
        }

        public String getRemarkT() {
            return remarkT;
        }

        public void setRemarkT(String remarkT) {
            this.remarkT = remarkT;
        }

        public String getGetter() {
            return getter;
        }

        public void setGetter(String getter) {
            this.getter = getter;
        }

        public String getDPNO() {
            return DPNO;
        }

        public void setDPNO(String DPNO) {
            this.DPNO = DPNO;
        }

        public Double getTotal() { return total; }

        public void setTotal(Double total) { this.total = total; }

        public String getPaymed() { return paymed; }

        public void setPaymed(String paymed) { this.paymed = paymed; }

    }
}
