package com.vbitx.jero.api.jasper;

import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by scheaman on 8/17/16.
 */
@Service
public class InvoiceDocumentService extends DocumentService {

    private static final Logger logger = LoggerFactory.getLogger(InvoiceDocumentService.class);

    private static final String template = "InvoiceHori.jrxml";

    public InvoiceDocumentService() throws JRException {
        super(template);
    }

    public ByteArrayOutputStream generatePdf(InvoiceDocumentData data) throws JRException {
        return super.generatePdf(data.parameters, data.itemData);
    }

    public static class InvoiceDocumentData {

        private Map parameters = new HashMap();
        private List<InvoiceDocumentItemData> itemData;

        public InvoiceDocumentData(String seller, String sellerAddress, String branch,
                                   String taxID, String buyer, String buyerAddress,
                                   String documentDate, String documentId, String transportationPeriod,
                                   String paymentDueDate, String stringInt, List<InvoiceDocumentItemData> itemData) {
            // Set up parameters for Jasper


            parameters.put("Seller", seller);
            parameters.put("SellerAddress", sellerAddress);
            parameters.put("Branch", branch);
            parameters.put("TaxID", taxID);
            parameters.put("Buyer", buyer);
            parameters.put("BuyerAddress", buyerAddress);
            parameters.put("DocumentDate", documentDate);
            parameters.put("DocumentID",documentId);
            parameters.put("TransportationPeriod", transportationPeriod);
            parameters.put("PaymentDueDate", paymentDueDate);
            parameters.put("StringInt", stringInt);

            // Set up datasource for Jasper
            this.itemData = itemData;
        }

        public List<InvoiceDocumentItemData> getItemData() { return itemData; }

        public void setItemData(List<InvoiceDocumentItemData> itemData) { this.itemData = itemData; }

        public Map getParameters() { return parameters; }

        public void setParameters(Map parameters) { this.parameters = parameters; }

    }

    public static class InvoiceDocumentItemData {
        private String transportationID ;
        private String transportationDate;
        private String customerRetrieveDate;
        private String invoiceNo;
        private Double total;


        public InvoiceDocumentItemData(String transportationID, String transportationDate, String customerRetrieveDate, String invoiceNo, Double total) {
            this.setTransportationID(transportationID);
            this.setTransportationDate(transportationDate);
            this.setCustomerRetrieveDate(customerRetrieveDate);
            this.setInvoiceNo(invoiceNo);
            this.setTotal(total);
        }

        public String getTransportationID() {
            return transportationID;
        }

        public void setTransportationID(String transportationID) { this.transportationID = transportationID; }

        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }

        public String getInvoiceNo() {
            return invoiceNo;
        }

        public void setInvoiceNo(String invoiceNo) {
            this.invoiceNo = invoiceNo;
        }

        public String getCustomerRetrieveDate() {
            return customerRetrieveDate;
        }

        public void setCustomerRetrieveDate(String customerRetrieveDate) { this.customerRetrieveDate = customerRetrieveDate; }

        public String getTransportationDate() {
            return transportationDate;
        }

        public void setTransportationDate(String transportationDate) {
            this.transportationDate = transportationDate;
        }
    }
}
