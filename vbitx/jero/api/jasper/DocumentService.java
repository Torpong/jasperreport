package com.vbitx.jero.api.jasper;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.io.ByteArrayOutputStream;
import java.util.Collection;
import java.util.Map;

/**
 * Created by scheaman on 8/17/16.
 */
public abstract class DocumentService {

    protected JasperReport jasperReport;

    public DocumentService(String templateName) throws JRException {
        jasperReport = JasperCompileManager.compileReport(templateName);
    }

    public ByteArrayOutputStream generatePdf(Map parameters, Collection<?> data) throws JRException {
        JasperPrint jasperPrint =
                JasperFillManager.fillReport(jasperReport, parameters, new JRBeanCollectionDataSource(data));
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, buffer);
        return buffer;


    }

}
