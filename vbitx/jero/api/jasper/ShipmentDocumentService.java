package com.vbitx.jero.api.jasper;

import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bossy on 29/8/2559.
 */
@Service
public class ShipmentDocumentService extends DocumentService {

    private static final Logger logger = LoggerFactory.getLogger(ShipmentDocumentService.class);

    private static final String template = "Shipment1.jrxml";

    public ShipmentDocumentService() throws JRException {
        super(template);
    }

    public ByteArrayOutputStream generatePdf(ShipmentDocumentData data) throws JRException {
        return super.generatePdf(data.parameters, data.itemData);
    }

    public static class ShipmentDocumentData {

        private Map parameters = new HashMap();
        private List<ShipmentDocumentItemData> itemData;

        public ShipmentDocumentData(String carrierName, String licensePlate, String remark,
                                            String hubSourceName,String hubDestinationName, String shipmentNumber,
                                            String createdDate, List<ShipmentDocumentItemData> itemData) {
            // Set up parameters for Jasper
            parameters.put("Contacter", carrierName);
            parameters.put("Registration", licensePlate);
            parameters.put("Remark", remark);
            parameters.put("HubSource", hubSourceName);
            parameters.put("HubDes", hubDestinationName);
            parameters.put("ShipmentNo", shipmentNumber);
            parameters.put("CreateDate", createdDate);

            // Set up datasource for Jasper
            this.itemData = itemData;
        }

        public List<ShipmentDocumentItemData> getItemData() { return itemData; }

        public void setItemData(List<ShipmentDocumentItemData> itemData) { this.itemData = itemData; }

        public Map getParameters() { return parameters; }

        public void setParameters(Map parameters) { this.parameters = parameters; }

    }

    public static class ShipmentDocumentItemData {
        private String DPNO ;
        private String DPDate;
        private String sender;
        private String getter;
        private String remarkT;

        public ShipmentDocumentItemData(String DPNO, String DPDate, String sender, String getter, String remarkT) {
            this.setDPNO(DPNO);
            this.setDPDate(DPDate);
            this.setSender(sender);
            this.setGetter(getter);
            this.setRemarkT(remarkT);
        }

        public String getSender() {
            return sender;
        }

        public void setSender(String sender) {
            this.sender = sender;
        }

        public String getRemarkT() {
            return remarkT;
        }

        public void setRemarkT(String remarkT) {
            this.remarkT = remarkT;
        }

        public String getGetter() {
            return getter;
        }

        public void setGetter(String getter) {
            this.getter = getter;
        }

        public String getDPDate() {
            return DPDate;
        }

        public void setDPDate(String DPDate) {
            this.DPDate = DPDate;
        }

        public String getDPNO() {
            return DPNO;
        }

        public void setDPNO(String DPNO) { this.DPNO = DPNO; }


    }
}
