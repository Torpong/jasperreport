package com.vbitx.jero.api.jasper;

import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bossy on 29/8/2559.
 */
@Service
public class PayInSlipDocumentService extends DocumentService {

    private static final Logger logger = LoggerFactory.getLogger(PayInSlipDocumentService.class);

    private static final String template = "Pay-InSlip.jrxml";

    public PayInSlipDocumentService() throws JRException {
        super(template);
    }

    public ByteArrayOutputStream generatePdf(PayInSlipDocumentData data) throws JRException {
        return super.generatePdf(data.parameters, data.itemData);
    }

    public static class PayInSlipDocumentData {

        private Map parameters = new HashMap();
        private List<PayInSlipDocumentItemData> itemData;

        public PayInSlipDocumentData(String hubName, String transferNumber, String createdDate,
                                     List<PayInSlipDocumentItemData> itemData) {
            // Set up parameters for Jasper
            parameters.put("hub", hubName);
            parameters.put("TransferNo", transferNumber);
            parameters.put("CreateDate", createdDate);

            // Set up datasource for Jasper
            this.itemData = itemData;
        }

        public List<PayInSlipDocumentItemData> getItemData() { return itemData; }

        public void setItemData(List<PayInSlipDocumentItemData> itemData) { this.itemData = itemData; }

        public Map getParameters() { return parameters; }

        public void setParameters(Map parameters) { this.parameters = parameters; }

    }

    public static class PayInSlipDocumentItemData {
        private String DPNO ;
        private String paymed;
        private String paydate;
        private String getter;
        private String customer;
        private double transferFee;
        private double recrieveFee;



        public PayInSlipDocumentItemData(String DPNO, String paymed, String paydate, String getter, String customer, double transferFee, double recrieveFee) {
            this.setDPNO(DPNO);
            this.setPaymed(paymed);
            this.setPaydate(paydate);
            this.setGetter(getter);
            this.setCustomer(customer);
            this.setTransferFee(transferFee);
            this.setRecrieveFee(recrieveFee);
        }

        public String getDPNO() {
            return DPNO;
        }

        public void setDPNO(String DPNO) {
            this.DPNO = DPNO;
        }

        public String getPaymed() {
            return paymed;
        }

        public void setPaymed(String paymed) {
            this.paymed = paymed;
        }

        public String getPaydate() {
            return paydate;
        }

        public void setPaydate(String paydate) {
            this.paydate = paydate;
        }

        public String getGetter() {
            return getter;
        }

        public void setGetter(String getter) {
            this.getter = getter;
        }

        public String getCustomer() {
            return customer;
        }

        public void setCustomer(String customer) {
            this.customer = customer;
        }

        public double getTransferFee() {
            return transferFee;
        }

        public void setTransferFee(double transferFee) {
            this.transferFee = transferFee;
        }

        public double getRecrieveFee() {
            return recrieveFee;
        }

        public void setRecrieveFee(double recrieveFee) {
            this.recrieveFee = recrieveFee;
        }

    }
}
