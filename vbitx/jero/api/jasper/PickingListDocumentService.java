package com.vbitx.jero.api.jasper;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bossy on 29/8/2559.
 */
@Service
public class PickingListDocumentService extends DocumentService {

    private static final Logger logger = LoggerFactory.getLogger(PickingListDocumentService.class);

    private static final String template = "PickingList.jrxml";

    public PickingListDocumentService() throws JRException {
        super(template);
    }

    public ByteArrayOutputStream generatePdf(PickingListDocumentData data) throws JRException {
        return super.generatePdf(data.parameters, data.itemData);
    }

    public static class PickingListDocumentData {

        private Map parameters = new HashMap();
        private List<PickingListDocumentItemData> itemData;

        public PickingListDocumentData(String carrierName, String licensePlate, String remark,
                                       String hubSourceName, String hubDestinationName, String shipmentNumber,
                                       String createdDate, List<PickingListDocumentItemData> itemData) {
            // Set up parameters for Jasper
            parameters.put("Contacter", carrierName);
            parameters.put("Registration", licensePlate);
            parameters.put("Remark", remark);
            parameters.put("HubSource", hubSourceName);
            parameters.put("HubDes", hubDestinationName);
            parameters.put("ShipmentNo", shipmentNumber);
            parameters.put("CreateDate", createdDate);

            // Set up datasource for Jasper
            this.itemData = itemData;
        }

        public List<PickingListDocumentItemData> getItemData() { return itemData; }

        public void setItemData(List<PickingListDocumentItemData> itemData) { this.itemData = itemData; }

        public Map getParameters() { return parameters; }

        public void setParameters(Map parameters) { this.parameters = parameters; }

    }




    public static class PickingListDocumentItemData {
        private String sender;
        private String getter;
        private String DPNO;
        private JRBeanCollectionDataSource itemsJRBean;

        public PickingListDocumentItemData(String sender, String getter, String DPNO, List<subtableData> data) {
            this.setDPNO(DPNO);
            this.setGetter(getter);
            this.setSender(sender);
            this.itemsJRBean = new JRBeanCollectionDataSource(data);
        }
        public JRBeanCollectionDataSource getItemsJRBean() {
            return itemsJRBean;
        }

        public void setItemsJRBean(JRBeanCollectionDataSource itemsJRBean) {
            this.itemsJRBean = itemsJRBean;
        }

        public String getSender() {
            return sender;
        }

        public void setSender(String sender) {
            this.sender = sender;
        }

        public String getGetter() {
            return getter;
        }

        public void setGetter(String getter) {
            this.getter = getter;
        }

        public String getDPNO() {
            return DPNO;
        }

        public void setDPNO(String DPNO) {
            this.DPNO = DPNO;
        }


    }
    public static class subtableData {
        private String productType;
        private String product;
        private Integer number;
        private String unit;
        private String remark;

        public subtableData(String remark, String unit, String product, String productType, Integer number){
            this.setProductType(productType);
            this.setRemark(remark);
            this.setProduct(product);
            this.setNumber(number);
            this.setUnit(unit);
        }

        public Integer getNumber() { return number; }

        public void setNumber(Integer number) { this.number = number; }

        public String getRemark() { return remark; }

        public void setRemark(String remark) { this.remark = remark; }

        public String getUnit() { return unit; }

        public void setUnit(String unit) { this.unit = unit; }

        public String getProduct() { return product; }

        public void setProduct(String product) { this.product = product; }

        public String getProductType() { return productType; }

        public void setProductType(String productType) { this.productType = productType; }

    }
}
