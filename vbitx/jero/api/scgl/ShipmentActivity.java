package com.vbitx.jero.api.scgl;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * @author scheaman
 */
@Embeddable
public class ShipmentActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    private DataSource dataSource = DataSource.UNKNOWN;

    @Temporal(TemporalType.TIMESTAMP)
    private Date actual = null;

    @Temporal(TemporalType.TIMESTAMP)
    private Date plan = null;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified = null;

    protected ShipmentActivity() {
        // empty
    }

    public ShipmentActivity(DataSource dataSource, Date actual, Date plan, Date lastModified) {
        this.dataSource = dataSource;
        this.actual = actual;
        this.plan = plan;
        this.lastModified = lastModified;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Date getActual() {
        return actual;
    }

    public void setActual(Date actual) {
        this.actual = actual;
    }

    public Date getPlan() {
        return plan;
    }

    public void setPlan(Date plan) {
        this.plan = plan;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * @author scheaman
     */
    public enum DataSource {
        UNKNOWN         (0),
        SCGL_FEED       (1),
        MANUAL_UPLOAD   (2),
        RESERVED_2      (3),
        RESEVERD_3      (4);

        private final int dataSourceCode;

        DataSource(int dataSourceCode) {
            this.dataSourceCode = dataSourceCode;
        }

        public int getDataSourceCode() {
            return this.dataSourceCode;
        }
    }
}
