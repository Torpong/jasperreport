package com.vbitx.jero.api.scgl;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author sun
 * @author scheaman
 * @version 1.0.2
 */
@Entity
public class ShipmentActivityMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private long id;

    private Activity activity;

    @Temporal(TemporalType.TIME)
    private Date defaultPlan;

    protected ShipmentActivityMaster() {
        //empty
    }

    public ShipmentActivityMaster(Activity activity) {
        this.activity = activity;
    }

    public long getId() {
        return id;
    }

    public Date getDefaultPlan() {
        return defaultPlan;
    }

    public void setDefaultPlan(Date defaultPlan) {
        this.defaultPlan = defaultPlan;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity sa) {
        this.activity = sa;
    }

    /**
     * @author scheaman
     */
    public enum Activity {
        OPEN,
        TENDER,
        TENDER_ACCEPT,
        INBOUND_ORIGIN,
        GR,
        GI,
        OUTBOUND_ORIGIN,
        INBOUND_DESTINATION,
        OUTBOUND_DESTINATION,
        DELIVERY,
        DOC_RETURN,
    }
}
