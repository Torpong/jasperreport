/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.scgl.service;

import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author scheaman
 */
@Service
public class ConstantService {

    @Value("${scgl.defaultDateFormat}")
    public String defaultDateFormat = "yyyy-MM-dd HH:mm:ss";

    @Value("${scgl.cdcCode1}")
    private String cdcCode1 = "NO-1200000003";

    @Value("${scgl.cdcCode2}")
    private String cdcCode2 = null;

    @Value("${scgl.cdcCode3}")
    private String cdcCode3 = null;

    public Set<String> cdcCode;

    public ConstantService() {
        cdcCode = new HashSet<>();
        if (cdcCode1 != null) cdcCode.add(cdcCode1);
        if (cdcCode2 != null) cdcCode.add(cdcCode2);
        if (cdcCode3 != null) cdcCode.add(cdcCode3);
    }

    /**
     *
     * @author scheaman
     */
    public enum CsvFieldType {

        DELIVERY_NUMBER             ("DELIVERY_NUMBER"),
        NO_OF_LEG                   ("TOTOAL_OF_LEG"),
        LEG_ID                      ("LEG_ID"),
        SHIPMENT_CREATE_DATE        ("SHIPMENT_CREATE_DATE"),
        SHIPMENT_NUMBER             ("SHIPMENT_NUMBER"),
        DRIVER_NAME                 ("DRIVER_NAME"),
        CARRIER_CODE                ("CARRIER_CODE"),
        CARRIER_NAME                ("CARRIER_NAME"),
        TRUCK_LICENSE               ("TRUCK_LICENSE"),
        SHIPPING_NAME               ("SHIPPING_NAME"),
        SHIPPING_POINT              ("SHIPPING_POINT"),
        SHIPTO_NAME                 ("SHIPTO_NAME"),
        SOLDTO_NAME                 ("SOLDTO_NAME"),
        ORDER_NUMBER                ("ORDER_NUMBER"),
        PO_NUMBER                   ("PO_NUMBER"),
        OPEN                        ("ACTUAL_OPEN"),
        TENDER                      ("ACTUAL_TENDER"),
        TENDER_ACCEPT               ("ACTUAL_TENDER_ACCEPT"),
        INBOUND_ORIGIN              ("ACTUAL_INBOUND_ORIGIN"),
        GI                          ("ACTUAL_GI_DATE"),
        OUTBOUND_ORIGIN             ("ACTUAL_OUTBOUND_ORIGIN"),
        INBOUND_DESTINATION         ("ACTUAL_INBOUND_DESTINATION"),
        GR                          ("ACTUAL_GR_DATE"),
        OUTBOUND_DESTINATION        ("ACTUAL_OUTBOUND_DESTINATION"),
        DELIVERY                    ("ACTUAL_DELIVERY_DATE"),
        DOC_RETURN                  ("ACTUAL_DOCUMENT_RETURN"),
        PLAN_TENDER                 ("PLAN_TENDER"),
        PLAN_TENDER_ACCEPT          ("PLAN_TENDER_ACCEPT"),
        PLAN_INBOUND_ORIGIN         ("PLAN_INBOUND_ORIGIN"),
        PLAN_GI                     ("PLAN_GI_DATE"),
        PLAN_OUTBOUND_ORIGIN        ("PLAN_OUTBOUND_ORIGIN"),
        PLAN_INBOUND_DESTINATION    ("PLAN_INBOUND_DESTINATION"),
        PLAN_GR                     ("PLAN_GR_DATE"),
        PLAN_OUTBOUND_DESTINATION   ("PLAN_OUTBOUND_DESTINATION"),
        PLAN_DELIVERY               ("PLAN_DELIVERY_DATE"),
        PLAN_DOC_RETURN             ("PLAN_DOCUMENT_RETURN"),
        LOAD_UTILIZATION            ("LOAD_UTILIZATION"),
        TRUCK_TYPE                  ("TRUCK_TYPE");

        private final String jsonString;

        CsvFieldType(String jsonString) {
            this.jsonString = jsonString;
        }

        public String getJsonString() {
            return jsonString;
        }
    }
}
