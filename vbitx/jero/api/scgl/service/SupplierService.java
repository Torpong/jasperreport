package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.Supplier;
import com.vbitx.jero.api.scgl.repository.SupplierRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sun on 6/13/2016 AD.
 */
@Transactional
@Service
public class SupplierService {
    @Autowired
    private SupplierRepository supplierRepository;

    public List<Supplier> listAll(){
        return supplierRepository.findAll();
    }

    public Supplier getById(long id) {
        return supplierRepository.findOne(id);
    }

}
