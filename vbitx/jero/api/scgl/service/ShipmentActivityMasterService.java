package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.ShipmentActivityMaster;
import com.vbitx.jero.api.scgl.repository.ShipmentActivityMasterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;

/**
 * Created by sun on 6/13/2016 AD.
 */
@Transactional
@Service
public class ShipmentActivityMasterService {
    @Autowired
    private ShipmentActivityMasterRepository shipmentActivityMasterRepository;

    public List<ShipmentActivityMaster> listAll(){
        return shipmentActivityMasterRepository.findAll();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public ShipmentActivityMaster create(ShipmentActivityMaster.Activity activity, Date datePlan){
        ShipmentActivityMaster sam = new ShipmentActivityMaster(activity);
        sam.setDefaultPlan(datePlan);
        return shipmentActivityMasterRepository.save(sam);
    }

    public ShipmentActivityMaster update(long id, Date datePlan){
        ShipmentActivityMaster sam = shipmentActivityMasterRepository.findOne(id);
        if (datePlan != null ) {
            sam.setDefaultPlan(datePlan);
        }
        return sam;
    }

    public ShipmentActivityMaster getById(long id){
        return shipmentActivityMasterRepository.findOne(id);
    }

    public ShipmentActivityMaster getByShipmentActivity(ShipmentActivityMaster.Activity activity){
        return shipmentActivityMasterRepository.findFirstByActivity(activity);
    }
}