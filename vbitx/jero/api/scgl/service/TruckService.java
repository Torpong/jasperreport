package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.*;
import com.vbitx.jero.api.scgl.repository.CarrierRepository;
import com.vbitx.jero.api.scgl.repository.ShipmentRepository;
import com.vbitx.jero.api.scgl.repository.TruckRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.vbitx.jero.api.scgl.repository.GpsRepository;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.time.DateUtils;

/**
 * Created by sun on 6/13/2016 AD.
 */
@Transactional
@Service
public class TruckService {

    @Autowired
    private TruckRepository truckRepository;

    @Autowired
    private CarrierRepository carrierRepository;

    @Autowired
    private ShipmentRepository shipmentRepository;
    
    @Autowired
    private GpsRepository gpsRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Truck create(String license, String carrierCode) {
        Carrier carrier = carrierRepository.findByCode(carrierCode);
        if (carrier == null || license == null || license.length() == 0) return null;
        Truck truck = truckRepository.findByLicense(license);
        if (truck == null) {
            truck = new Truck(license);
            truck = truckRepository.save(truck);
        }
        truck.setCarrier(carrier);
        return truck;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Truck create(String license) {
        if (license == null || license.length() == 0) return null;
        Truck truck = truckRepository.findByLicense(license);
        if (truck == null) {
            truck = new Truck(license);
            truck = truckRepository.save(truck);
        }
        return truck;
    }

    public List<Truck> listAll(){
        return truckRepository.findAll();
    }

    public Truck getByLicense(String license){
        return truckRepository.findByLicense(license);
    }

    public List<Truck> listByCarrier(Carrier carrier){
        return truckRepository.findByCarrier(carrier);
    }

    public void listByLongitudeAndLatitude(String latitude, String longtitide){
        //Not yet implemented
    }
    
    public List<Truck> getActiveTrucks() {
        List<Shipment> shipments = shipmentRepository.findByCompleted(false);
        List<Truck> trucks = new ArrayList<>();
        for (Shipment s : shipments) {
            trucks.add(s.getTruck());
        }
        return trucks;
    }
    
    public Integer getCurrentSpeed(String license) {
        Truck t = truckRepository.findByLicense(license);
        if (t == null) return null;
        Gps lastKnownGps = t.getLastKnownGps();
        if (lastKnownGps == null) return null;
        // check if we have fresh data
        Date fiveMinutesAgo = DateUtils.addMinutes(new Date(), -5);
        if (lastKnownGps.getTimestamp().compareTo(fiveMinutesAgo) < 0) {
            return null;
        }
        return lastKnownGps.getSpeed();
    }
    
    public Double getCurrentAverageSpeed(String license, int minute) {
        Truck t = truckRepository.findByLicense(license);
        if (t == null) return null;
        Date end = new Date();
        Date start = DateUtils.addMinutes(end, -minute);
        if (end == null || start == null) return null;
        Set<Gps> gpses = gpsRepository.findByTruckAndTsBetween(t, start, end);
        if (gpses == null || gpses.isEmpty()) return null;
        double avg = 0f;
        for (Gps gps : gpses) {
            avg += gps.getSpeed();
        }
        return avg/gpses.size();
    }
    
    public Double getAverageSpeed(String license, long shipmentId) {
        Truck t = truckRepository.findByLicense(license);
        Shipment s = shipmentRepository.findOne(shipmentId);
        if (t == null || s == null || s.isInTransit()) return null;
        Date end = s.getInboundDestination().getActual();
        Date start = s.getOutboundOrigin().getActual();
        if (end == null || start == null) return null;
        Set<Gps> gpses = gpsRepository.findByTruckAndTsBetween(t, start, end);
        if (gpses == null || gpses.isEmpty()) return null;
        double avg = 0f;
        for (Gps gps : gpses) {
            avg += gps.getSpeed();
        }
        return avg/gpses.size();
    }

}
