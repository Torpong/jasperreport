/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.scgl.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import static java.util.Calendar.DAY_OF_MONTH;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.vbitx.jero.api.scgl.ShipmentActivity;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author scheaman
 */
@Transactional
@Service
public class DataService {

    private static final Logger logger = LoggerFactory.getLogger(DataService.class);

    private static final int MAX_RETRY = 3;

    @Value("${scgl.gps.url}")
    private String truckApiUrl = "";

    @Value("${scgl.gps.token}")
    private String truckApiToken = "";

    @Autowired
    private SupplierInvoiceService supplierInvoiceService;

    @Autowired
    private ShipmentService shipmentService;

    @Autowired
    private GpsService gpsLocationService;

    @Autowired
    private ConstantService constantService;

    @Transactional(propagation = Propagation.NEVER)
    public synchronized void processDN(String dn,
                                       Map<String, List<Map<String, String>>> data,
                                       ShipmentActivity.DataSource dataSource) {
        logger.info("Processing " + dn);
        // update shipments
        boolean dnSuccess = true;
        for (Map<String, String> row : data.get(dn)) {
            boolean rowSuccess = false;
            for (int i = 0; i < MAX_RETRY; i++) {
                try {
                    shipmentService.populateEntity(row, dataSource);
                    shipmentService.updateTruckSpeed(row);
                    rowSuccess = true;
                    break;
                } catch (Exception ex) {
                    logger.warn("Cannot add shipment. Retrying...");
                    ex.printStackTrace();
                }
            }
            if (!rowSuccess) {
                dnSuccess = false;
            }
        }
        if (dnSuccess) {
            // update supplier invoice
            try {
                supplierInvoiceService.update(dn);
                supplierInvoiceService.cacheProcessMonitoringDN(dn);
            } catch (Exception ex) {
                logger.error("Cannot update DN");
            }
        } else {
            logger.error("Cannot add one or more shipments.");
        }
    }
    
    @Transactional(propagation = Propagation.NEVER)
    public void rebuildDailyCache(Map<String, List<Map<String, String>>> data) {
        logger.info("Rebuilding cache...");
        Set<Date> allDates = new HashSet<>();
        for (String dn : data.keySet()) {
            for (Map<String, String> row : data.get(dn)) {
                try {
                    if (row.get(ConstantService.CsvFieldType.SHIPMENT_CREATE_DATE.getJsonString()).length() > 0) {
                        Date createdDate = new SimpleDateFormat(constantService.defaultDateFormat)
                                .parse(row.get(ConstantService.CsvFieldType.SHIPMENT_CREATE_DATE.getJsonString()));
                        createdDate = DateUtils.truncate(createdDate, DAY_OF_MONTH);
                        allDates.add(createdDate);
                    }
                } catch (ParseException pe) {
                    logger.warn("Unable to parse date.");
                }
            }
        }
        for (Date date : allDates) {
            logger.info("caching " + date.toString());
            supplierInvoiceService.cacheProcessMonitoringByDate(date);
        }
        logger.info("Rebuilding cache done!");
    }

    @Scheduled(fixedDelayString = "${scgl.gps.interval}")
    public void updateTruckGps() {
        logger.info("GPS start");
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(truckApiUrl);

        // add request header
        request.addHeader("Authorization", "Bearer " + truckApiToken);
        try {
            HttpResponse response = client.execute(request);

            if (response.getStatusLine().getStatusCode() != 200) {
                logger.error("Unable to retrieve GPS data.");
                return;
            }

            // convert to Map
            Map<String, Object> result = new ObjectMapper().readValue(response.getEntity().getContent(), HashMap.class);
            for (Object entry : (List) result.get("truck_data")) {
                Map<String, Object> truckData = (Map) entry;
                String license = (String) truckData.get("license");
                if (license != null && license.length() > 0) {
                    double lat = getDoubleValueOrZero(truckData.get("lat"));
                    double longt = getDoubleValueOrZero(truckData.get("long"));
                    int speed = (Integer) truckData.get("speed");
                    try {
                        gpsLocationService.create(
                                license,
                                lat,
                                longt,
                                speed,
                                new Date(getEpoch(truckData.get("timestamp"))));
                        //logger.info("GPS truck: " + truckData.get("license"));
                    } catch (Exception ee) {
                        logger.warn("Fail to create a truck GPS for " + truckData);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Cannot process GPS data");
            e.printStackTrace();
        }
        logger.info("GPS done");
    }

    private double getDoubleValueOrZero(Object d) {
        double result = 0.0;
        try {
            result = ((Double) d);
            return result;
        } catch (Exception e) {
            // Not a double
        }
        try {
            result = (double) ((Integer) d);
            return result;
        } catch (Exception e) {
            // Not an int
        }
        return result;
    }

    private long getEpoch(Object d) {
        long ts = (Integer) d;
        return ts * 1000;
    }

}
