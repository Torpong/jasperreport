package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.Customer;
import com.vbitx.jero.api.scgl.Supplier;
import com.vbitx.jero.api.scgl.SupplierOrder;
import com.vbitx.jero.api.scgl.repository.SupplierOrderRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sun on 6/13/2016 AD.
 */
@Transactional
@Service
public class SupplierOrderService {
    @Autowired
    private SupplierOrderRepository supplierOrderRepository;

    public List<SupplierOrder> listAll() {
        return supplierOrderRepository.findAll();
    }

    public SupplierOrder findByOrderNumber(String number) {
        return supplierOrderRepository.findByNumber(number);
    }

    public List<SupplierOrder> listByCustomer(Customer customer) {
        return supplierOrderRepository.findByCustomer(customer);
    }

    public List<SupplierOrder> listBySupplier(Supplier supplier) {
        return supplierOrderRepository.findBySupplier(supplier);
    }


}
