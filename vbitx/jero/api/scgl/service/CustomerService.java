package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.Customer;
import com.vbitx.jero.api.scgl.repository.CustomerRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sun on 6/13/2016 AD.
 */
@Transactional
@Service
public class CustomerService {
    
    @Autowired
    private CustomerRepository customerRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Customer create(String name) {
        if (name == null || name.length() == 0) return null;
        Customer customer = customerRepository.findFirstByName(name);
        if (customer == null) {
            customer = new Customer(name);
            customer = customerRepository.save(customer);
        }
        return customer;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Customer update(long id, String name, String address){
        Customer customer = customerRepository.findOne(id);
        if (customer != null) {
            customer.setName(name);
            customer.setAddress(address);
        }
        return customer;
    }
    
    public Customer findById(long id){
        return customerRepository.findOne(id);
    }

    public List<Customer> listAll() {
        return customerRepository.findAll();
    }
    
}
