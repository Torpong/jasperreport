package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.ApiKey;
import com.vbitx.jero.api.scgl.repository.ApiKeyRepository;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author scheaman
 */
@Transactional
@Service
public class ApiKeyService {
    
    @Autowired
    private ApiKeyRepository apiKeyRepository;
    
    public boolean isValid(String key) {
        ApiKey apiKey = apiKeyRepository.findBySecretKey(key);
        if (apiKey != null) {
            Date expiry = apiKey.getExpiry();
            // check if expired
            if (expiry == null || expiry.after(new Date())) {
                return true;
            }
        }
        return false;
    }
}