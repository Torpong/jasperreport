package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.Station;
import com.vbitx.jero.api.scgl.repository.StationRepository;
import java.util.List;

import com.vbitx.jero.api.user.User;
import com.vbitx.jero.api.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sun on 6/13/2016 AD.
 */
@Transactional
@Service
public class StationService {
    
    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private UserRepository userRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Station create(String name) {
        Station station = stationRepository.findFirstByName(name);
        if (station == null) {
            station = new Station(name);
            station = stationRepository.save(station);
        }
        return station;
    }

    public List<Station> listAll(){
        return stationRepository.findAll();
    }

    public Station getByCode(String code){
        return stationRepository.findFirstByCode(code);
    }

    public Station getById(long id){
        return stationRepository.findOne(id);
    }

    public boolean canAccess(String username, long stationId) {
        Station station = stationRepository.findOne(stationId);
        User user = userRepository.findByUsername(username);
        if (station == null || user == null) return false;
        return station.getAllowedUsers().contains(user);
    }
}
