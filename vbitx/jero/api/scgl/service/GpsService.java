package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.Gps;
import com.vbitx.jero.api.scgl.Truck;
import com.vbitx.jero.api.scgl.repository.GpsRepository;
import com.vbitx.jero.api.scgl.repository.TruckRepository;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sun on 6/13/2016 AD.
 */
@Service
public class GpsService {

    @Autowired
    private GpsRepository gpsRepository;

    @Autowired
    private TruckRepository truckRepository;
    
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Gps create(String license, double latitude,
                      double longitude, int speed, Date timestamp) {
        Truck truck = truckRepository.findByLicense(license);
        if (truck == null) return null;
        Gps gps = new Gps("", latitude, longitude, speed, timestamp, truck);
        gps = gpsRepository.save(gps);
        truck.setLastKnownGps(gps);
        return gpsRepository.save(gps);
    }

}
