package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.Driver;
import com.vbitx.jero.api.scgl.repository.DriverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sun on 6/13/2016 AD.
 */
@Transactional
@Service
public class DriverService {
    @Autowired
    private DriverRepository driverRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Driver create(String name) {
        if (name == null || name.length() == 0) return null;
        Driver driver = driverRepository.findFirstByName(name);
        if (driver == null) {
            driver = new Driver(name);
            driver = driverRepository.save(driver);
        }
        return driver;
    }
   
}