package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.Carrier;
import com.vbitx.jero.api.scgl.repository.CarrierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sun on 6/13/2016 AD.
 */
@Transactional
@Service
public class CarrierService {
    
    @Autowired
    private CarrierRepository carrierRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Carrier create(String code, String name) {
        if (code == null || code.length() == 0) return null;
        Carrier carrier = carrierRepository.findByCode(code);
        if (carrier == null) { 
            carrier = new Carrier(code);
            carrier = carrierRepository.save(carrier);
        }
        carrier.setName(name);
        return carrier;
    }

    public Page<Carrier> listAll(int page, int limit){
        PageRequest pageRequest = new PageRequest(page - 1, limit);
        return carrierRepository.findAll(pageRequest);
    }

    public Carrier getByCode(String code){
        return carrierRepository.findByCode(code);
    }

}