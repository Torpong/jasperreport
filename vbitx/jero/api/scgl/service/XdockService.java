package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.*;
import com.vbitx.jero.api.scgl.repository.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author scheaman
 */
@Transactional
@Service
public class XdockService {

    private static final Logger logger = LoggerFactory.getLogger(XdockService.class);

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private ShipmentRepository shipmentRepository;

    @Autowired
    private IssuedShipmentRepository issuedShipmentRepository;

    @Autowired
    private XdockInvoiceRepository xdockInvoiceRepository;

    @Autowired
    private XdockShipmentRepository xdockShipmentRepository;

    @Autowired
    private IssuedShipmentService issuedShipmentService;

    public Page<IssuedShipment.IssuedShipmentDetail> getInboundShipmentsByStationId(long stationId, int page, int limit) {
        Station station = stationRepository.findOne(stationId);
        if (station == null) return null;
        PageRequest pageRequest = new PageRequest(page - 1, limit);
        Page<IssuedShipment> pageIssuedShipment = shipmentRepository.findIssuedShipmentToReceive(station, pageRequest);
        List<IssuedShipment.IssuedShipmentDetail> content = new ArrayList<>();
        for (IssuedShipment s : pageIssuedShipment.getContent()) {
            content.add(issuedShipmentService.getIssuedShipmentDetail(s.getNumber()));
        }
        Page<IssuedShipment.IssuedShipmentDetail> details = new PageImpl<IssuedShipment.IssuedShipmentDetail>(
                content, pageRequest, pageIssuedShipment.getTotalElements());
        return details;
    }

    public void receiveShipment(long stationId, String shippingNumber, Date date) {
        Station station = stationRepository.findOne(stationId);
        if (station == null) return;
        IssuedShipment issuedShipment = issuedShipmentRepository.findByNumber(shippingNumber);
        if (issuedShipment == null) return;
        // List all DNs of this shipment
        // For each DN: create XdockInvoice
        for (Shipment s : issuedShipment.getShipments()) {
            Shipment nextShipment = shipmentRepository.findFirstBySupplierInvoiceDeliveryNumberAndOriginStation(s.getDeliveryNumber(), station);
            if (nextShipment == null) {
                logger.error("GR error. Cannot find next shipment");
                return;
            }
            XdockInvoice invoice = xdockInvoiceRepository.findByStationAndDeliveryNumber(station, s.getDeliveryNumber());
            if (invoice == null) {
                invoice = new XdockInvoice(s.getDeliveryNumber(), station, nextShipment);
                invoice = xdockInvoiceRepository.save(invoice);
            }
            // Set GR timestamp in the xdock system
            invoice.setReceiveDate(date);
            // --- Tracking ----
            s.setInTransit(false);
            s.getInboundDestination().setActual(date);
            nextShipment.getInboundOrigin().setActual(date);
            nextShipment.getGr().setActual(date);
        }
    }

    public XdockShipment createXdockShipment(long stationId, String driverName, String truckLicense) {
        Station station = stationRepository.findById(stationId);
        if (station == null) return null;
        XdockShipment xdockShipment = new XdockShipment(station);
        xdockShipment.setDriver(driverName);
        xdockShipment.setLicense(truckLicense);
        xdockShipmentRepository.save(xdockShipment);
        return xdockShipment;
    }

    public XdockShipment updateXdockShipment(long xdockShipmentId, String driverName, String truckLicense) {
        XdockShipment xdockShipment = xdockShipmentRepository.findOne(xdockShipmentId);
        xdockShipment.setDriver(driverName);
        xdockShipment.setLicense(truckLicense);
        return xdockShipment;
    }

    public XdockInvoice addXdockInvoiceToShipment(String xdockInvoiceId, long xdockShipmentId) {
        XdockShipment xdockShipment = xdockShipmentRepository.findOne(xdockShipmentId);
        if (xdockShipment == null) return null;
        XdockInvoice xdockInvoice = xdockInvoiceRepository.findByDeliveryNumber(xdockInvoiceId);
        if (xdockInvoice == null) return null;
        xdockInvoice.setXdockShipment(xdockShipment);
        // set issuedShipment
        String issuedId = xdockShipment.getIdAsString();
        IssuedShipment issuedShipment = issuedShipmentRepository.findByNumber(issuedId);
        if (issuedShipment == null) {
            issuedShipment = new IssuedShipment(issuedId);
            issuedShipment = issuedShipmentRepository.save(issuedShipment);
        }
        issuedShipment.setIssuedBy(IssuedShipment.IssuedBy.XDOCK);
        xdockInvoice.getTrackingShipment().setIssuedShipment(issuedShipment);
        return xdockInvoice;
    }

    public XdockInvoice removeXdockInvoiceFromShipment(String xdockInvoiceId, long xdockShipmentId) {
        XdockShipment xdockShipment = xdockShipmentRepository.findOne(xdockShipmentId);
        if (xdockShipment == null) return null;
        XdockInvoice xdockInvoice = xdockInvoiceRepository.findByDeliveryNumber(xdockInvoiceId);
        if (xdockInvoice == null) return null;
        if (xdockInvoice.getXdockShipment() == xdockShipment) {
            xdockInvoice.setXdockShipment(null);
            // unset issuedShipment
            Shipment s = xdockInvoice.getTrackingShipment();
            if (s != null &&
                    s.getIssuedShipment() != null &&
                    s.getIssuedShipment().getIssuedBy() == IssuedShipment.IssuedBy.XDOCK) {
                s.setIssuedShipment(null);
            }
        }
        return xdockInvoice;
    }

    public List<XdockInvoice> listAvailableXdockInvoices(long stationId) {
        Station station = stationRepository.findOne(stationId);
        if (station == null) return null;
        Set<XdockInvoice> invoices = xdockInvoiceRepository.findByStationAndXdockShipmentIsNull(station);
        return new ArrayList<>(invoices);
    }

    public void issueShipment(long xdockShipmentId, Date date) {
        XdockShipment xdockShipment = xdockShipmentRepository.findOne(xdockShipmentId);
        if (xdockShipment == null) return;
        for (XdockInvoice invoice : xdockShipment.getInvoices()) {
            // set GI timestamp in the xdock system
            invoice.setIssueDate(date);
            // set GI timestamp in the tracking system
            invoice.getTrackingShipment().getGi().setActual(date);
        }
        // Mark as issued
        xdockShipment.setIssued(true);
        xdockShipmentRepository.save(xdockShipment);
    }

    public Page<XdockShipment> getUnissuedXdockShipment(long stationId, int page, int limit) {
        Station station = stationRepository.findOne(stationId);
        if (station == null) return null;
        PageRequest pageRequest = new PageRequest(page - 1, limit);
        return xdockShipmentRepository.findByStationAndIssuedFalse(station, pageRequest);
    }

    public XdockShipment getXdockShipment(long xdockShipmentId) {
        return xdockShipmentRepository.findOne(xdockShipmentId);
    }
}
