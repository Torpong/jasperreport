package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.IssuedShipment;
import com.vbitx.jero.api.scgl.Shipment;
import com.vbitx.jero.api.scgl.repository.IssuedShipmentRepository;
import com.vbitx.jero.api.scgl.repository.ShipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author scheaman
 */
@Transactional
@Service
public class IssuedShipmentService {

    @Autowired
    private IssuedShipmentRepository issuedShipmentRepository;

    @Autowired
    private ShipmentRepository shipmentRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public IssuedShipment create(String number) {
        if (number == null || number.length() == 0) return null;
        IssuedShipment issuedShipment = issuedShipmentRepository.findByNumber(number);
        if (issuedShipment == null) {
            issuedShipment = new IssuedShipment(number);
            issuedShipment = issuedShipmentRepository.save(issuedShipment);
        }
        return issuedShipment;
    }


    public Page<IssuedShipment.IssuedShipmentDetail> listScgShipment(int page, int limit, Date from, Date to, String status) {
        PageRequest pageRequest = new PageRequest(page - 1, limit);
        Page<IssuedShipment> pageIssuedShipment;

        if (status.toLowerCase().equals("all")) {
            pageIssuedShipment = shipmentRepository.findFilteredIssuedShipments(IssuedShipment.IssuedBy.SCGL, from, to, pageRequest);
        } else if (status.toLowerCase().equals("completed")) {
            pageIssuedShipment = shipmentRepository.findFilteredIssuedShipments(IssuedShipment.IssuedBy.SCGL, from, to, true, pageRequest);
        } else {
            pageIssuedShipment = shipmentRepository.findFilteredIssuedShipments(IssuedShipment.IssuedBy.SCGL, from, to, false, pageRequest);
        }

        List<IssuedShipment.IssuedShipmentDetail> content = new ArrayList<>();
        for (IssuedShipment s : pageIssuedShipment.getContent()) {
            content.add(getIssuedShipmentDetail(s.getNumber()));
        }
        Page<IssuedShipment.IssuedShipmentDetail> details = new PageImpl<IssuedShipment.IssuedShipmentDetail>(
                content, pageRequest, pageIssuedShipment.getTotalElements());
        return details;
    }

    public IssuedShipment.IssuedShipmentDetail getIssuedShipmentDetail(String shippingNumber) {
        IssuedShipment issuedShipment = issuedShipmentRepository.findByNumber(shippingNumber);
        if (issuedShipment == null)
            return null;
        IssuedShipment.IssuedShipmentDetail issuedShipmentDetail = new IssuedShipment.IssuedShipmentDetail();
        issuedShipmentDetail.number = shippingNumber;
        issuedShipmentDetail.issuedBy = issuedShipment.getIssuedBy().name();
        boolean first = true;
        for (Shipment s : issuedShipment.getShipments()) {
            if (first) {
                issuedShipmentDetail.truck = s.getTruck();
                issuedShipmentDetail.driver = s.getDriver();
                issuedShipmentDetail.created = s.getCreated();
                issuedShipmentDetail.origin = s.getOriginStation();
                issuedShipmentDetail.destination = s.getDestinationStation();
                issuedShipmentDetail.shipmentActivities = s.getShipmentActivities();
                first = false;
            }
            issuedShipmentDetail.supplierInvoice.add(s.getSupplierInvoice());
            issuedShipmentDetail.references.add(s.getId());
        }
        return issuedShipmentDetail;
    }



}