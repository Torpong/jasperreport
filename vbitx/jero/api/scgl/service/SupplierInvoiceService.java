package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.*;
import com.vbitx.jero.api.scgl.repository.ShipmentRepository;
import com.vbitx.jero.api.scgl.repository.SupplierInvoiceRepository;

import java.util.*;

import static java.util.Calendar.DAY_OF_MONTH;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sun on 6/13/2016 AD.
 */
@Transactional
@Service
public class SupplierInvoiceService {

    private static final Logger logger = LoggerFactory.getLogger(SupplierInvoiceService.class);

    @Autowired
    private ShipmentRepository shipmentRepository;

    @Autowired
    private SupplierInvoiceRepository supplierInvoiceRepository;

    @Autowired
    private ConstantService constantService;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private TaskExecutor executor;

    public Set<SupplierInvoice> listByCurrentShipment(Shipment shipment){
        return supplierInvoiceRepository.findByCurrentShipment(shipment);
    }

    public Page<SupplierInvoice> listByPurchaseNumber(String poNum, int page, int limit){
        PageRequest pageRequest = new PageRequest(page - 1, limit);
        Page<SupplierInvoice> supplierInvoices = supplierInvoiceRepository.findBySupplierOrderPurchaseOrderNumber(poNum, pageRequest);
        return supplierInvoices;
    }

    public List<SupplierInvoice> listAll() {
        return supplierInvoiceRepository.findAll();
    }

    public Page<SupplierInvoice> listAll(int page, int limit) {
        PageRequest pageRequest = new PageRequest(page - 1, limit);
        return supplierInvoiceRepository.findAll(pageRequest);
    }

    public SupplierInvoice findByDeliveryNumber(String dn) {
        return supplierInvoiceRepository.findByDeliveryNumber(dn);
    }

    @Transactional
    public SupplierInvoice create(String dn) {
        if (dn == null || dn.length() == 0) return null;
        SupplierInvoice invoice = supplierInvoiceRepository.findByDeliveryNumber(dn);
        if (invoice == null) {
            invoice = new SupplierInvoice(dn, null);
            invoice = supplierInvoiceRepository.save(invoice);
        }
        return invoice;
    }

    public List<Object> listAllProcessMonitoring() {
        List<SupplierInvoice> supplierInvoices = this.listAll();
        List<Object> response = new ArrayList<>();
        for (SupplierInvoice si : supplierInvoices) {
            response.add(findByProcessMonitoringDN(si.getDeliveryNumber()));
        }
        return response;
    }

    public Object listProcessMonitoringByDate(Date date) {
        // Reset date to midnight
        date = DateUtils.truncate(date, DAY_OF_MONTH);
        String key = DateUtils.truncate(date, DAY_OF_MONTH).toString();
        Object response = cacheService.get(key);
        if (response == null) {
            response = cacheProcessMonitoringByDate(date);
        }
        return response;
    }

    public Object cacheProcessMonitoringByDate(Date date) {
        // Reset date to midnight
        date = DateUtils.truncate(date, DAY_OF_MONTH);
        // get the next midnight
        Date endDate = DateUtils.addDays(date, 1);
        // check date-level cache
        String key = DateUtils.truncate(date, DAY_OF_MONTH).toString();
        List<Object> data = new ArrayList<>();
        List<String> allDns = supplierInvoiceRepository.findDeliveryNumberByCreatedBetween(date, endDate);
        for (String dn : allDns) {
            Object each = findByProcessMonitoringDN(dn);
            if (each != null) {
                data.add(each);
            }
        }
        executor.execute(new CachePutTask(key, data, cacheService));
        return data;
    }

    public void update(String dn) {
        SupplierInvoice invoice = supplierInvoiceRepository.findByDeliveryNumber(dn);
        if (invoice != null) {
            SortedSet<Shipment> shipments = invoice.getShipments();
            Shipment currentShipment = shipments.first();
            Iterator<Shipment> it = shipments.iterator();
            int i = 0;
            while (it.hasNext()) {
                Shipment s = it.next();
                if (i >= invoice.getMaxLegNumber()) {
                    it.remove();
                    shipmentRepository.delete(s);
                } else {
                    if (s.getOutboundOrigin().getActual() != null) {
                        currentShipment = s;
                    }
                }
                i++;
            }
            invoice.setCurrentShipment(currentShipment);

            // update current leg number
            int currentLegNo = Integer.parseInt(currentShipment.getLegId()) / 100;
            invoice.setCurrentLegNumber(currentLegNo);

            // compute shipment status
            if (currentLegNo == invoice.getMaxLegNumber() && currentShipment.isCompleted()) {
                invoice.setStatus(SupplierInvoice.Status.Completed);
            } else {
                invoice.setStatus(SupplierInvoice.Status.Ongoing);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void cacheProcessMonitoringDN(String dn) {
        SupplierInvoice si = supplierInvoiceRepository.findByDeliveryNumber(dn);
        if (si == null) return;

        List<Shipment> shipments = new ArrayList<>(si.getShipments());
        Map<String, Object> response = new HashMap<>();

        response.put(SupplierInvoice.SINGULAR, si.asMap());
        response.put("currentShipment", si.getCurrentShipment()==null?null:si.getCurrentShipment().asMap());

        Map<String, Object> kpiMap = new HashMap<>();
        kpiMap.put("cdc", populateCDCMap(shipments));
        kpiMap.put("lpc", populateLPCMap(shipments));
        kpiMap.put("lineHaul", populateLinehaulMap(si, shipments));
        kpiMap.put("lastMile", populateLastMileMap(shipments));
        kpiMap.put("score", populateScore(kpiMap));
        response.put("kpi", kpiMap);

        // update cache
        cacheService.put(dn, response);
    }

    public Object findByProcessMonitoringDN(String dn) {
        // Always get from cache
        return cacheService.get(dn);
    }

//----- private helpers -----//
    private Map<String, Object> populateScore(Map<String, Object> kpiMap) {
        Map<String, Object> response = new HashMap<>();
        List<String> statusNames = new ArrayList<>();
        // lpc
        statusNames.clear();
        statusNames.add("tenderOnTimeStatus");
        response.put("lpcScore", this.calculateScore(((Map<String, Object>) kpiMap.get("lpc")), statusNames));

        // cdc
        statusNames.clear();
        statusNames.add("productInboundOnTimeStatus");
        statusNames.add("truckOutboundOnTimeStatus");
        statusNames.add("gIOnTimeStatus");
        statusNames.add("gROnTimeStatus");
        response.put("cdcScore", this.calculateScore(((Map<String, Object>) kpiMap.get("cdc")), statusNames));

        //line hual
        statusNames.clear();
        statusNames.add("utilizationStatus");
        response.put("lineHaulScore", this.calculateScore(((Map<String, Object>) kpiMap.get("lineHaul")), statusNames));

        // last mile
        statusNames.clear();
        statusNames.add("inboundOnTimeStatus");
        statusNames.add("outboundOnTimeStatus");
        statusNames.add("deliveryOnTimeStatus");
        statusNames.add("docReturnOnTimeStatus");
        response.put("lastMileScore", this.calculateScore(((Map<String, Object>) kpiMap.get("lastMile")), statusNames));

        return response;
    }

    private Map<String, Object> calculateScore(Map<String, Object> kpiMap, List<String> keys) {
        Map<String, Object> response = new HashMap<>();
        float totalScore = 0.0f;
        float currentScore = 0.0f;

        for (String key : keys) {
            Boolean val = (Boolean) kpiMap.get(key);
            if (val != null) {
                totalScore += 10;
                if (val.booleanValue()) {
                    currentScore += 10;
                }
            }
        }
        response.put("totalScore", totalScore);
        response.put("currentScore", currentScore);
        response.put("percentageScore", totalScore == 0.0f ? null : (currentScore / totalScore) * 100);
        return response;
    }

    private Map<String, Object> populateLinehaulMap(SupplierInvoice si, List<Shipment> shipments) {
        Map<String, Object> lineHaulMap = new HashMap<>();
        Shipment lineHaulShipment = null;
        for (Shipment s : shipments) {
            if (constantService.cdcCode.contains(s.getOriginStation().getCode())) {
                lineHaulShipment = s;
                break;
            }
        }

        Double speed = null;
        Double utilization = null;

        if (lineHaulShipment != null) {
            speed = lineHaulShipment.getAverageTruckSpeed();
            utilization = lineHaulShipment.getUtilization();
        }

        lineHaulMap.put("averageTruckSpeed", speed);
        lineHaulMap.put("utilization", utilization);
        lineHaulMap.put("utilizationStatus", utilization==null?null:utilization >= 80.0);
        return lineHaulMap;
    }

    private Map<String, Object> populateCDCMap(List<Shipment> shipments) {
        Map<String, Object> cdcMap = new HashMap<>();

        for (Shipment s : shipments) {
            if (constantService.cdcCode.contains(s.getOriginStation().getCode())) {
                cdcMap.put("INBOUND_ORIGIN", s.getInboundOrigin());
                cdcMap.put("GR", s.getGr());
                cdcMap.put("GI", s.getGi());
                cdcMap.put("OUTBOUND_ORIGIN", s.getOutboundOrigin());
                break;
            }
        }

        // inbound time
        cdcMap.put("productInboundOnTimeStatus", isOnTime((ShipmentActivity) cdcMap.get("INBOUND_ORIGIN")));
        // GR time
        cdcMap.put("gROnTimeStatus", isOnTime((ShipmentActivity) cdcMap.get("GR")));
        // GI time
        cdcMap.put("gIOnTimeStatus", isOnTime((ShipmentActivity) cdcMap.get("GI")));
        // outbound time
        cdcMap.put("truckOutboundOnTimeStatus", isOnTime((ShipmentActivity) cdcMap.get("OUTBOUND_ORIGIN")));
        return cdcMap;
    }

    private Map<String, Object> populateLPCMap(List<Shipment> shipments) {
        Map<String, Object> lpcMap = new HashMap<>();

        // Find very first shipment
        Shipment firstShipment = shipments.get(0);
        lpcMap.put("TENDER", firstShipment.getTender());

        // tender time
        lpcMap.put("tenderOnTimeStatus", isOnTime((ShipmentActivity) lpcMap.get("TENDER")));
        return lpcMap;
    }

    private Map<String, Object> populateLastMileMap(List<Shipment> shipments) {
        Map<String, Object> lastMileMap = new HashMap<>();

        // Find very last shipment
        Shipment lastShipment = shipments.get(shipments.size() - 1);

        if (!constantService.cdcCode.contains(lastShipment.getOriginStation().getCode())) {
            lastMileMap.put("INBOUND_ORIGIN", lastShipment.getInboundOrigin());
            lastMileMap.put("OUTBOUND_ORIGIN", lastShipment.getOutboundOrigin());

            // inbound time
            lastMileMap.put("inboundOnTimeStatus", isOnTime((ShipmentActivity) lastMileMap.get("INBOUND_ORIGIN")));

            // outbound time
            lastMileMap.put("outboundOnTimeStatus", isOnTime((ShipmentActivity) lastMileMap.get("OUTBOUND_ORIGIN")));
        }

        lastMileMap.put("DELIVERY", lastShipment.getDelivery());
        lastMileMap.put("DOC_RETURN", lastShipment.getDocReturn());

        // delivery time
        lastMileMap.put("deliveryOnTimeStatus", isOnTime((ShipmentActivity) lastMileMap.get("DELIVERY")));

        // doc return time
        lastMileMap.put("docReturnOnTimeStatus", isOnTime((ShipmentActivity) lastMileMap.get("DOC_RETURN")));
        return lastMileMap;
    }

    private Boolean isOnTime(ShipmentActivity activity) {
        if (activity == null || activity.getActual() == null || activity.getPlan() == null) return null;
        return !(activity.getActual().after(activity.getPlan()));
    }


    private class CachePutTask implements Runnable {

        String key;
        Collection data;
        CacheService cs;

        public CachePutTask(String key, Collection data, CacheService cs) {
            this.key = key;
            this.data = data;
            this.cs = cs;
        }

        @Override
        public void run() {
            cs.put(key, data);
        }
    }
}
