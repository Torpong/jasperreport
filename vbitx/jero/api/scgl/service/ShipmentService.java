package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.*;
import com.vbitx.jero.api.scgl.repository.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sun on 6/13/2016 AD.
 */
@Transactional
@Service
public class ShipmentService {

    private static final Logger logger = LoggerFactory.getLogger(ShipmentService.class);
    
    // Repositories
    @Autowired
    private CarrierRepository carrierRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private DriverRepository driverRepository;

    @Autowired
    private ShipmentRepository shipmentRepository;

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private SupplierRepository supplierRepository;

    @Autowired
    private IssuedShipmentRepository issuedShipmentRepository;
    
    @Autowired
    private SupplierInvoiceRepository supplierInvoiceRepository;

    @Autowired
    private SupplierOrderRepository supplierOrderRepository;

    @Autowired
    private TruckRepository truckRepository;

    // Services
    @Autowired
    private ConstantService constantService;
    
    @Autowired
    private TruckService truckService;

    // TODO: Recheck logic
    @Transactional
    public Page<Shipment> listByDestinationStation(long stationId, int page, int limit) {
        Station station = stationRepository.findOne(stationId);
        List<Shipment> shipmentList = shipmentRepository.findByDestinationStation(station);
        PageRequest pageRequest = new PageRequest(page - 1, limit);
        Page<Shipment> responseShipments = new PageImpl<>(shipmentList, pageRequest, shipmentList.size());
        return responseShipments;
    }
    
    public void receiveShipmentAtDestination(long shipmentId) {
        // TODO: 7/25/2016 AD  --->  update activity (inbound_destination?)
        Shipment shipment = shipmentRepository.findOne(shipmentId);
//        shipment.lastActivity()
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void populateEntity(Map<String, String> entry, ShipmentActivity.DataSource dataSource) throws ParseException {
        Shipment shipment = populateShipmentInformation(entry);
        shipment = populateShipmentActivity(shipment, entry, dataSource);
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateTruckSpeed(Map<String, String> entry) throws ParseException {
        String dn = entry.get(ConstantService.CsvFieldType.DELIVERY_NUMBER.getJsonString());
        String legId = entry.get(ConstantService.CsvFieldType.LEG_ID.getJsonString());
        Shipment shipment = shipmentRepository.findFirstBySupplierInvoiceDeliveryNumberAndLegId(dn, legId);
        if (shipment == null || shipment.getTruck() == null) return;
        Double speed = truckService.getAverageSpeed(shipment.getTruck().getLicense(), shipment.getId());
        shipment.setAverageTruckSpeed(speed);
    }
    

    public Page<Shipment> listAll(int page, int limit) {
        PageRequest pageRequest = new PageRequest(page - 1, limit);
        return shipmentRepository.findAll(pageRequest);
    }

    public Shipment getById(long id) {
        return shipmentRepository.findOne(id);
    }

    public List<Shipment> searchByDeliveryNumber(String dnNum) {
        SupplierInvoice invoice = supplierInvoiceRepository.findByDeliveryNumber(dnNum);
        if (invoice != null) return new ArrayList<>(invoice.getShipments());
        else return null;
    }

    private Shipment populateShipmentInformation(Map<String, String> entry) throws ParseException {
        Date createdDate = null;
        if (entry.get(ConstantService.CsvFieldType.SHIPMENT_CREATE_DATE.getJsonString()).length() > 0) {
            createdDate = new SimpleDateFormat(constantService.defaultDateFormat)
                    .parse(entry.get(ConstantService.CsvFieldType.SHIPMENT_CREATE_DATE.getJsonString()));
        }

        String driverName = entry.get(ConstantService.CsvFieldType.DRIVER_NAME.getJsonString());
        Driver driver = null;
        if (driverName != null && driverName.length() > 0) {
            driver = driverRepository.findFirstByName(driverName);
            if (driver == null) {
                driver = new Driver(driverName);
                driver = driverRepository.save(driver);
            }
        }

        String carrierCode = entry.get(ConstantService.CsvFieldType.CARRIER_CODE.getJsonString());
        String carrierName = entry.get(ConstantService.CsvFieldType.CARRIER_NAME.getJsonString());
        Carrier carrier = null;
        if (carrierCode != null && carrierCode.length() > 0) {
            carrier = carrierRepository.findByCode(carrierCode);
            if (carrier == null) {
                carrier = new Carrier(carrierCode);
                carrier = carrierRepository.save(carrier);
            }
            carrier.setName(carrierName);
        }

        String truckLicense = entry.get(ConstantService.CsvFieldType.TRUCK_LICENSE.getJsonString());
        Truck truck = null;
        if (truckLicense != null && truckLicense.length() > 0) {
            truck = truckRepository.findByLicense(truckLicense);
            if (truck == null) {
                truck = new Truck(truckLicense);
                truck = truckRepository.save(truck);
            }
            truck.setCarrier(carrier);
        }

        String shippingName = entry.get(ConstantService.CsvFieldType.SHIPPING_NAME.getJsonString());
        String shippingPoint = entry.get(ConstantService.CsvFieldType.SHIPPING_POINT.getJsonString());
        Station origin = stationRepository.findFirstByName(shippingName);
        if (origin == null) {
            origin = new Station(shippingName);
            origin = stationRepository.save(origin);
        }
        origin.setCode(shippingPoint);

        String shipToName = entry.get(ConstantService.CsvFieldType.SHIPTO_NAME.getJsonString());
        Station destination = stationRepository.findFirstByName(shipToName);
        if (destination == null) {
            destination = new Station(shipToName);
            destination = stationRepository.save(destination);
        }

        String soldToName = entry.get(ConstantService.CsvFieldType.SOLDTO_NAME.getJsonString());
        Customer customer = customerRepository.findFirstByName(soldToName);
        if (customer == null) {
            customer = new Customer(soldToName);
            customer = customerRepository.save(customer);
        }

        Supplier supplier = supplierRepository.findFirstByName(shippingName);
        if (supplier == null) {
            supplier = new Supplier(shippingName);
            supplier = supplierRepository.save(supplier);
        }

        String orderNumber = entry.get(ConstantService.CsvFieldType.ORDER_NUMBER.getJsonString());
        String poNumber = entry.get(ConstantService.CsvFieldType.PO_NUMBER.getJsonString());
        SupplierOrder supplierOrder = supplierOrderRepository.findByNumber(orderNumber);
        if (supplierOrder == null) {
            supplierOrder = new SupplierOrder(orderNumber);
            supplierOrder = supplierOrderRepository.save(supplierOrder);
        }
        supplierOrder.setPurchaseOrderNumber(poNumber);
        supplierOrder.setCustomer(customer);
        supplierOrder.setSupplier(supplier);

        String dn = entry.get(ConstantService.CsvFieldType.DELIVERY_NUMBER.getJsonString());
        // Try reading number of legs.
        // Default value is 1
        int noOfLeg = 1;
        try {
            noOfLeg = Integer.parseInt(entry.get(ConstantService.CsvFieldType.NO_OF_LEG.getJsonString()));
        } catch (NumberFormatException e) {
            // use default value
        }
        SupplierInvoice supplierInvoice = supplierInvoiceRepository.findByDeliveryNumber(dn);
        if (supplierInvoice == null) {
            supplierInvoice = new SupplierInvoice(dn, supplierOrder);
            supplierInvoice = supplierInvoiceRepository.save(supplierInvoice);
        }
        supplierInvoice.setCreated(createdDate);
        supplierInvoice.setMaxLegNumber(noOfLeg);

        String shipmentNumber = entry.get(ConstantService.CsvFieldType.SHIPMENT_NUMBER.getJsonString());
        IssuedShipment issuedShipment = null;
        if (shipmentNumber != null && shipmentNumber.length() > 0) {
            issuedShipment = issuedShipmentRepository.findByNumber(shipmentNumber);
            if (issuedShipment == null) {
                issuedShipment = new IssuedShipment(shipmentNumber);
                issuedShipment = issuedShipmentRepository.save(issuedShipment);
            }
            issuedShipment.setIssuedBy(IssuedShipment.IssuedBy.SCGL);
        }

        String legId = entry.get(ConstantService.CsvFieldType.LEG_ID.getJsonString());
        String truckType = entry.get(ConstantService.CsvFieldType.TRUCK_TYPE.getJsonString());
        String utilization = entry.get(ConstantService.CsvFieldType.LOAD_UTILIZATION.getJsonString());
        Shipment shipment = shipmentRepository.findFirstBySupplierInvoiceDeliveryNumberAndLegId(dn, legId);
        if (shipment == null) {
            shipment = new Shipment(supplierInvoice, legId);
            shipment = shipmentRepository.save(shipment);
        }
        shipment.setCarrier(carrier);
        shipment.setCreated(createdDate);
        shipment.setDestinationStation(destination);
        shipment.setDriver(driver);
        shipment.setIssuedShipment(issuedShipment);
        shipment.setOriginStation(origin);
        shipment.setSupplierInvoice(supplierInvoice);
        shipment.setTruck(truck);
        shipment.setTruckType(truckType);
        if (utilization != null && !utilization.isEmpty()) {
            shipment.setUtilization(Double.parseDouble(utilization));
        }
        return shipment;
    }

    private Shipment populateShipmentActivity(Shipment shipment,
                                              Map<String, String> entry,
                                              ShipmentActivity.DataSource dataSource) throws ParseException {
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(constantService.defaultDateFormat);

        // Open
        Date actual = parseDate(entry.get(ConstantService.CsvFieldType.OPEN.getJsonString()),dateFormat);
        Date plan = null;
        shipment.setOpen(new ShipmentActivity(dataSource, actual, plan, now));

        // Tender
        actual = parseDate(entry.get(ConstantService.CsvFieldType.TENDER.getJsonString()),dateFormat);
        plan = parseDate(entry.get(ConstantService.CsvFieldType.PLAN_TENDER.getJsonString()),dateFormat);
        shipment.setTender(new ShipmentActivity(dataSource, actual, plan, now));

        // Tender Accept
        actual = parseDate(entry.get(ConstantService.CsvFieldType.TENDER_ACCEPT.getJsonString()),dateFormat);
        plan = parseDate(entry.get(ConstantService.CsvFieldType.PLAN_TENDER_ACCEPT.getJsonString()),dateFormat);
        shipment.setTenderAccept(new ShipmentActivity(dataSource, actual, plan, now));

        // Inbound Origin
        actual = parseDate(entry.get(ConstantService.CsvFieldType.INBOUND_ORIGIN.getJsonString()),dateFormat);
        plan = parseDate(entry.get(ConstantService.CsvFieldType.PLAN_INBOUND_ORIGIN.getJsonString()),dateFormat);
        shipment.setInboundOrigin(new ShipmentActivity(dataSource, actual, plan, now));

        // GI
        actual = parseDate(entry.get(ConstantService.CsvFieldType.GI.getJsonString()),dateFormat);
        plan = parseDate(entry.get(ConstantService.CsvFieldType.PLAN_GI.getJsonString()),dateFormat);
        shipment.setGi(new ShipmentActivity(dataSource, actual, plan, now));

        // Outbound Origin
        actual = parseDate(entry.get(ConstantService.CsvFieldType.OUTBOUND_ORIGIN.getJsonString()),dateFormat);
        plan = parseDate(entry.get(ConstantService.CsvFieldType.PLAN_OUTBOUND_ORIGIN.getJsonString()),dateFormat);
        shipment.setOutboundOrigin(new ShipmentActivity(dataSource, actual, plan, now));

        // Inbound Destination
        actual = parseDate(entry.get(ConstantService.CsvFieldType.INBOUND_DESTINATION.getJsonString()),dateFormat);
        plan = parseDate(entry.get(ConstantService.CsvFieldType.PLAN_INBOUND_DESTINATION.getJsonString()),dateFormat);
        shipment.setInboundDestination(new ShipmentActivity(dataSource, actual, plan, now));

        // GR
        actual = parseDate(entry.get(ConstantService.CsvFieldType.GR.getJsonString()),dateFormat);
        plan = parseDate(entry.get(ConstantService.CsvFieldType.PLAN_GR.getJsonString()),dateFormat);
        shipment.setGr(new ShipmentActivity(dataSource, actual, plan, now));

        // Outbound Destination
        actual = parseDate(entry.get(ConstantService.CsvFieldType.OUTBOUND_DESTINATION.getJsonString()),dateFormat);
        plan = parseDate(entry.get(ConstantService.CsvFieldType.PLAN_OUTBOUND_DESTINATION.getJsonString()),dateFormat);
        shipment.setOutboundDestination(new ShipmentActivity(dataSource, actual, plan, now));

        // Delivery
        actual = parseDate(entry.get(ConstantService.CsvFieldType.DELIVERY.getJsonString()),dateFormat);
        plan = parseDate(entry.get(ConstantService.CsvFieldType.PLAN_DELIVERY.getJsonString()),dateFormat);
        shipment.setDelivery(new ShipmentActivity(dataSource, actual, plan, now));

        // Doc Return
        actual = parseDate(entry.get(ConstantService.CsvFieldType.DOC_RETURN.getJsonString()),dateFormat);
        plan = parseDate(entry.get(ConstantService.CsvFieldType.PLAN_DOC_RETURN.getJsonString()),dateFormat);
        shipment.setDocReturn(new ShipmentActivity(dataSource, actual, plan, now));

        // set completed flag
        shipment.setCompleted(shipment.getDocReturn().getActual() != null);

        // set in-transit flag
        shipment.setInTransit(
                shipment.getOutboundOrigin().getActual() != null &&
                shipment.getInboundDestination().getActual() == null);

        return shipment;
    }

    private Date parseDate(String dateString, SimpleDateFormat dateFormat) throws ParseException {
        if (dateString == null || dateString.isEmpty()) return null;
        return dateFormat.parse(dateString);
    }

}
