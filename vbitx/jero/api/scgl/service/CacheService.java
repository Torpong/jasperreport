package com.vbitx.jero.api.scgl.service;

import com.vbitx.jero.api.scgl.Cache;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.vbitx.jero.api.scgl.repository.CacheRepository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;

/**
 *
 * @author scheaman
 */
@Transactional
@Service
public class CacheService {
    
    @Autowired
    private CacheRepository cacheRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void put(String key, Object data) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream objOut = null;
        try {
            objOut = new ObjectOutputStream(out);
            objOut.writeObject(data);
        } catch (IOException ex) {
            // do nothing
        } finally {
            IOUtils.closeQuietly(objOut);
        }         
        Cache cache = cacheRepository.findByCacheKey(key);
        if (cache == null) {
            cache = new Cache(key, out.toByteArray());
            cacheRepository.save(cache);
        } else {
            cache.setCacheData(out.toByteArray());
            cache.setValid(true);
        }
    }

    @Transactional(readOnly = true)
    public Object get(String key) {
        Cache cache = cacheRepository.findByCacheKeyAndValidTrue(key);
        if (cache != null) {
            ObjectInputStream ois = null;
            try {
                ByteArrayInputStream bis = new ByteArrayInputStream(cache.getCacheData());
                ois = new ObjectInputStream(bis);
                Object outObj = ois.readObject();
                return outObj;
            } catch (IOException | ClassNotFoundException ex) {
                // do nothing
            } finally {
                IOUtils.closeQuietly(ois);
            }
        }
        // if any error, return null
        return null;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void invalidate(String key) {
        Cache cache = cacheRepository.findByCacheKey(key);
        if (cache != null) {
            cache.setValid(false);
        }
    }
}
