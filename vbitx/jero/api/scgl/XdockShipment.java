package com.vbitx.jero.api.scgl;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by scheaman on 8/9/16.
 */
@Entity
public class XdockShipment {

    @Id
    @GeneratedValue
    private long id;

    private boolean issued = false;

    private String driver;

    private String license;

    @ManyToOne
    @JoinColumn(name = "station_id")
    private Station station;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @OneToMany(mappedBy = "xdockShipment", orphanRemoval = true)
    private List<XdockInvoice> invoices;

    protected XdockShipment() {
        // empty
    }

    public XdockShipment(Station station) {
        this.station = station;
    }

    public boolean issued() {
        return issued;
    }

    public void setIssued(boolean issued) {
        this.issued = issued;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public List<XdockInvoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<XdockInvoice> invoices) {
        this.invoices = invoices;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public long getId() {
        return id;
    }

    @Transient
    public String getIdAsString() {
        return "XD" + StringUtils.leftPad(Long.toString(getId()),10,'0');
    }

}
