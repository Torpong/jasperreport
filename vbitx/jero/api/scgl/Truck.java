/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.scgl;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author gigadot
 * @author scheaman
 * @version 1.0.2
 */
@Entity
public class Truck implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(nullable = false)
    private String license;

    @ManyToOne
    @JoinColumn(name = "carrier_id")
    private Carrier carrier;

    @ManyToOne
    @JoinColumn(name = "gps_id")
    private Gps lastKnownGps;

    @Transient
    public static final String SINGULAR = Truck.class.getSimpleName().toLowerCase();

    @Transient
    public static final String PLURAL = SINGULAR + "s";

    protected Truck() {
        // empty
    }

    public Truck(String license) {
        this.license = license;
    }

    public String getLicense() {
        return license;
    }

    public Carrier getCarrier() {
        return carrier;
    }

    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    public Gps getLastKnownGps() {
        return lastKnownGps;
    }

    public void setLastKnownGps(Gps lastKnownGps) {
        this.lastKnownGps = lastKnownGps;
    }

    @JsonIgnore
    public Map asMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("license", getLicense());
        map.put("carrier", getCarrier() == null ? null : getCarrier().asMap());
        return map;
    }

}
