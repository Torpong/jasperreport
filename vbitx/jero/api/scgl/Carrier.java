/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.scgl;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author gigadot
 * @author scheaman
 * @version 1.0.2
 */
@Entity
public class Carrier implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(nullable = false)
    private String code;

    private String name;

    @Transient
    public static final String SINGULAR = Carrier.class.getSimpleName().toLowerCase();

    @Transient
    public static final String PLURAL = SINGULAR + "s";

    protected Carrier() {
        // empty
    }

    public Carrier(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Transient
    @JsonIgnore
    public Map asMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("code", getCode());
        map.put("name", getName());
        return map;
    }

}
