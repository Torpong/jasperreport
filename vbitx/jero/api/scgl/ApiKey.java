/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.scgl;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author scheaman
 * @version 1.0.2
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class ApiKey implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(nullable = false)
    private String secretKey;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date created = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    private Date expiry;

    protected ApiKey() {
        // empty
    }

    public ApiKey(String secret, Date expiry) {
        this.secretKey = secret;
        this.expiry = expiry;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public Date getExpiry() {
        return expiry;
    }

    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

}
