/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.scgl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author scheaman
 * @version 1.0.2
 */
@Entity
@Table(indexes = {
        @Index(columnList = "valid", name = "valid_idx"),
        @Index(columnList = "cacheKey,valid", name = "cache_key_valid_idx")
})
@EntityListeners(AuditingEntityListener.class)
public class Cache implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(nullable = false)
    private String cacheKey;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] cacheData;

    private boolean valid;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified;

    protected Cache() {
        // empty
    }

    public Cache(String cacheKey, byte[] cacheData) {
        this.cacheData = cacheData;
        this.cacheKey = cacheKey;
        this.valid = true;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    @JsonIgnore
    public byte[] getCacheData() {
        return cacheData;
    }

    public void setCacheData(byte[] cacheData) {
        this.cacheData = cacheData;
    }
}
