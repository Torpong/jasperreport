/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.scgl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vbitx.jero.api.user.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author gigadot
 * @author scheaman
 * @version 1.0.2
 */
@Entity
public class Station implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private long id;

    private String code;

    private String name;

    private String address;

    private String street;

    private String city;

    private String postcode;

    private String regionCode;

    private String regionName;

    @ManyToMany
    @JoinTable(
            name="user_station",
            joinColumns=@JoinColumn(name="station_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="user_id", referencedColumnName="id"))
    private Set<User> allowedUsers;

    @Transient
    public static final String SINGULAR = Station.class.getSimpleName().toLowerCase();

    @Transient
    public static final String PLURAL = SINGULAR + "s";

    protected Station() {
        // empty
    }

    public Station(String name) {
        this.name = name;
    }

    public Station(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    @JsonIgnore
    public Map asMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", getId());
        map.put("code", getCode());
        map.put("name", getName());
        map.put("address", getAddress());
        map.put("street", getStreet());
        map.put("city", getCity());
        map.put("postcode", getPostcode());
        map.put("regionCode", getRegionCode());
        map.put("regionName", getRegionName());
        return map;
    }

    @JsonIgnore
    public Set<User> getAllowedUsers() {
        return allowedUsers;
    }

    public void setAllowedUsers(Set<User> allowedUsers) {
        this.allowedUsers = allowedUsers;
    }
}
