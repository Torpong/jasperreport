/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.scgl;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author gigadot
 * @author scheaman
 */
@Entity
public class SupplierOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(nullable = false)
    private String number;

    private String purchaseOrderNumber;

    @ManyToOne
    @JoinColumn(name = "supplier_id")
    private Supplier supplier;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @Transient
    public static final String SINGULAR = SupplierOrder.class.getSimpleName().toLowerCase();

    @Transient
    public static final String PLURAL = SINGULAR + "s";

    protected SupplierOrder() {
    }

    public SupplierOrder(String number) {
        this.number = number;
    }

    public SupplierOrder(String number, String purchaseOrderNumber, Supplier supplier, Customer customer) {
        this.number = number;
        this.purchaseOrderNumber = purchaseOrderNumber;
        this.supplier = supplier;
        this.customer = customer;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Transient
    @JsonIgnore
    public Map asMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("number", getNumber());
        map.put("purchaseOrderNumber", getPurchaseOrderNumber());
        map.put("supplier", getSupplier() == null ? null : getSupplier().asMap());
        map.put("customer", getCustomer() == null ? null : getCustomer().asMap());
        return map;
    }

}
