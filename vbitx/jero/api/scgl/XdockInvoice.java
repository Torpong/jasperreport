package com.vbitx.jero.api.scgl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by scheaman on 8/9/16.
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class XdockInvoice {

    @Id
    private String deliveryNumber;

    @ManyToOne
    @JoinColumn(name = "station_id")
    private Station station;

    @ManyToOne
    @JoinColumn(name = "shipment_id")
    private Shipment trackingShipment;

    @Temporal(TemporalType.TIMESTAMP)
    private Date receiveDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date IssueDate;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @ManyToOne
    @JoinColumn(name = "xdocker_shipment_id")
    private XdockShipment xdockShipment;

    protected XdockInvoice() {
        // empty
    }

    public XdockInvoice(String deliveryNumber, Station station, Shipment shipment) {
        this.deliveryNumber = deliveryNumber;
        this.station = station;
        this.trackingShipment = shipment;
    }

    public String getDeliveryNumber() {
        return deliveryNumber;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Shipment getTrackingShipment() {
        return trackingShipment;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    public Date getIssueDate() {
        return IssueDate;
    }

    public void setIssueDate(Date issueDate) {
        IssueDate = issueDate;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @JsonIgnore
    public XdockShipment getXdockShipment() {
        return xdockShipment;
    }

    public void setXdockShipment(XdockShipment xdockShipment) {
        this.xdockShipment = xdockShipment;
    }
}
