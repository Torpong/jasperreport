/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.scgl;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author scheaman
 * @version 1.0.2
 */
@Entity
@Table(uniqueConstraints
        = @UniqueConstraint(columnNames = {"truck_id", "ts"}))
public class Gps implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private long id;

    private String location;

    private double latitude;

    private double longitude;

    private int speed;

    @Temporal(TemporalType.TIMESTAMP)
    private Date ts;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "truck_id", nullable = false)
    private Truck truck;

    protected Gps() {
        // empty
    }

    public Gps(String location, double latitude, double longitude,
               int speed, Date ts, Truck truck) {
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        this.ts = ts;
        this.truck = truck;
    }

    public long getId() {
        return id;
    }

    @JsonIgnore
    public Truck getTruck() {
        return truck;
    }

    public void setTruck(Truck truck) {
        this.truck = truck;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public Date getTimestamp() {
        return ts;
    }

    public void setTimestamp(Date ts) {
        this.ts = ts;
    }

}
