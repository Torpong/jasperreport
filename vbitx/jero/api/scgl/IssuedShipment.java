/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.scgl;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * @author scheaman
 */
@Entity
public class IssuedShipment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(nullable = false)
    private String number;

    private IssuedBy issuedBy;

    @OneToMany(mappedBy = "issuedShipment", orphanRemoval = true)
    private Set<Shipment> shipments = new HashSet<>();

    @Transient
    public static final String SINGULAR = IssuedShipment.class.getSimpleName().toLowerCase();

    @Transient
    public static final String PLURAL = SINGULAR + "s";

    protected IssuedShipment() {
        // empty
    }

    public IssuedShipment(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public IssuedBy getIssuedBy() {
        return issuedBy;
    }

    public void setIssuedBy(IssuedBy issuedBy) {
        this.issuedBy = issuedBy;
    }

    public Set<Shipment> getShipments() {
        return shipments;
    }

    public void setShipments(Set<Shipment> shipments) {
        this.shipments = shipments;
    }

    @Transient
    @JsonIgnore
    public Map asMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("number", getNumber());
        map.put("issueBy", getIssuedBy().name());
        return map;
    }

    /**
     * @author scheaman
     */
    public enum IssuedBy {
        SCGL(0),
        XDOCK(1),
        RESERVED_0(2),
        RESERVED_1(3),
        RESEVERD_2(4);

        private final int issuedByCode;

        IssuedBy(int issuedByCode) {
            this.issuedByCode = issuedByCode;
        }

        public int getShipmentStatusCode() {
            return this.issuedByCode;
        }
    }

    /**
     * Created by scheaman on 8/9/16.
     */
    public static class IssuedShipmentDetail {
        public String number;
        public String issuedBy;
        public Set<SupplierInvoice> supplierInvoice = new HashSet<>();
        public Set<Long> references = new HashSet<>();
        public Station origin;
        public Station destination;
        public Truck truck;
        public Driver driver;
        public Date created;
        public Map shipmentActivities;
    }
}
