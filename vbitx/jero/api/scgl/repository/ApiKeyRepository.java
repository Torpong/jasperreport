package com.vbitx.jero.api.scgl.repository;

import com.vbitx.jero.api.scgl.ApiKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * 
 * @author scheaman
 */
@RepositoryRestResource(exported = false)
public interface ApiKeyRepository extends JpaRepository<ApiKey, Long> {
    
    ApiKey findBySecretKey(String secretKey);
    
}
