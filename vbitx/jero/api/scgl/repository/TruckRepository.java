package com.vbitx.jero.api.scgl.repository;

import com.vbitx.jero.api.scgl.Carrier;
import com.vbitx.jero.api.scgl.Truck;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by sun on 6/13/2016 AD.
 */
@RepositoryRestResource(exported = false)
public interface TruckRepository extends JpaRepository<Truck, Long> {

    Truck findByLicense(String license);

    List<Truck> findByCarrier(Carrier carrier);

}
