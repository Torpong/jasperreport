package com.vbitx.jero.api.scgl.repository;

import com.vbitx.jero.api.scgl.*;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by sun on 6/13/2016 AD.
 */
@RepositoryRestResource(exported = false)
public interface SupplierOrderRepository extends JpaRepository<SupplierOrder, Long> {

    SupplierOrder findByNumber(String number);
    
    List<SupplierOrder> findByCustomer(Customer customer);
    
    List<SupplierOrder> findBySupplier(Supplier supplier);

}
