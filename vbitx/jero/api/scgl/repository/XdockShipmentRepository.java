package com.vbitx.jero.api.scgl.repository;

import com.vbitx.jero.api.scgl.Station;
import com.vbitx.jero.api.scgl.XdockShipment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * author scheaman
 */
@RepositoryRestResource(exported = false)
public interface XdockShipmentRepository extends JpaRepository<XdockShipment, Long> {

    Page<XdockShipment> findByStationAndIssuedFalse(Station station, Pageable pageable);

}
