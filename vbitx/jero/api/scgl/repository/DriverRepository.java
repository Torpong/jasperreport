package com.vbitx.jero.api.scgl.repository;

import com.vbitx.jero.api.scgl.Driver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by sun on 6/13/2016 AD.
 */
@RepositoryRestResource(exported = false)
public interface DriverRepository extends JpaRepository<Driver, Long> {

    Driver findFirstByName(String name);
    
}
