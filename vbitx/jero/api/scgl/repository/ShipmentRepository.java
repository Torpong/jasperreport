package com.vbitx.jero.api.scgl.repository;

import com.vbitx.jero.api.scgl.IssuedShipment;
import com.vbitx.jero.api.scgl.Shipment;
import com.vbitx.jero.api.scgl.ShipmentActivityMaster;
import com.vbitx.jero.api.scgl.Station;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by sun on 6/13/2016 AD.
 */
@RepositoryRestResource(exported = false)
public interface ShipmentRepository extends JpaRepository<Shipment, Long> {

    Shipment findFirstBySupplierInvoiceDeliveryNumberAndLegId(String dn, String LegId);

    Shipment findFirstBySupplierInvoiceDeliveryNumberAndOriginStation(String dn, Station station);

    List<Shipment> findByCompleted(boolean completed);

    List<Shipment> findByDestinationStation(Station station);

    Page<Shipment> findPaginationByCreatedBetweenAndCompleted(
            Date from, Date to, boolean completed, Pageable pageable);

    Page<Shipment> findPaginationByCreatedBetween(
            Date from, Date to, Pageable pageable);

    @Query(value = "select distinct s.issuedShipment from Shipment s " +
            "where s.issuedShipment IS NOT NULL and " +
            "s.destinationStation = ?1 and " +
            "s.inTransit = true and s.completed = false")
    Page<IssuedShipment> findIssuedShipmentToReceive(Station station, Pageable pageable);

    @Query(value = "select distinct s.issuedShipment from Shipment s " +
            "where s.issuedShipment IS NOT NULL and " +
            "s.issuedShipment.issuedBy = ?1")
    Page<IssuedShipment> findAllIssuedShipments(IssuedShipment.IssuedBy issuedBy, Pageable pageable);

    @Query(value = "select distinct s.issuedShipment from Shipment s " +
            "where s.issuedShipment IS NOT NULL and " +
            "s.issuedShipment.issuedBy = ?1 and " +
            "s.created >= ?2 and s.created <= ?3 and " +
            "s.completed = ?3")
    Page<IssuedShipment> findFilteredIssuedShipments(IssuedShipment.IssuedBy issuedBy,
                                                     Date from, Date to, boolean completed, Pageable pageable);

    @Query(value = "select distinct s.issuedShipment from Shipment s " +
            "where s.issuedShipment IS NOT NULL and " +
            "s.issuedShipment.issuedBy = ?1 and " +
            "s.created >= ?2 and s.created <= ?3")
    Page<IssuedShipment> findFilteredIssuedShipments(IssuedShipment.IssuedBy issuedBy,
                                                     Date from, Date to, Pageable pageable);

}
