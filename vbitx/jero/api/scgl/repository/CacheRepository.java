package com.vbitx.jero.api.scgl.repository;

import com.vbitx.jero.api.scgl.Cache;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * 
 * @author scheaman
 */
@RepositoryRestResource(exported = false)
public interface CacheRepository extends JpaRepository <Cache, Long> {
   
    Cache findByCacheKey(String cacheKey);
    
    Cache findByCacheKeyAndValidTrue(String cacheKey);

}
