package com.vbitx.jero.api.scgl.repository;

import com.vbitx.jero.api.scgl.Gps;
import com.vbitx.jero.api.scgl.Truck;
import java.util.Date;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * 
 * @author scheaman
 */
@RepositoryRestResource(exported = false)
public interface GpsRepository extends JpaRepository<Gps, Long> {

    Set<Gps> findByTruckAndTsBetween(Truck truck, Date start, Date end);
    
    //@Query("select avg(gps.speed) from Gps gps where gps.truck.id = (?1).id and gps.ts >= ?2 and gps.ts <= ?3")
    //Double findAverageSpeed(Truck truck, Date start, Date end);
    
}
