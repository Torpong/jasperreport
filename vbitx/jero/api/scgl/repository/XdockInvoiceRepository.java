package com.vbitx.jero.api.scgl.repository;

import com.vbitx.jero.api.scgl.Carrier;
import com.vbitx.jero.api.scgl.Station;
import com.vbitx.jero.api.scgl.Truck;
import com.vbitx.jero.api.scgl.XdockInvoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Set;

/**
 * author scheaman
 */
@RepositoryRestResource(exported = false)
public interface XdockInvoiceRepository extends JpaRepository<XdockInvoice, Long> {

    XdockInvoice findByStationAndDeliveryNumber(Station station, String dn);

    Set<XdockInvoice> findByStationAndXdockShipmentIsNull(Station station);

    XdockInvoice findByDeliveryNumber(String xdockInvoiceId);
}
