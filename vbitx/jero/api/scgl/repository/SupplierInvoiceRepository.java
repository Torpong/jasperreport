package com.vbitx.jero.api.scgl.repository;

import com.vbitx.jero.api.scgl.Shipment;
import com.vbitx.jero.api.scgl.Station;
import com.vbitx.jero.api.scgl.SupplierInvoice;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by sun on 6/13/2016 AD.
 */
@RepositoryRestResource(exported = false)
public interface SupplierInvoiceRepository extends JpaRepository<SupplierInvoice, Long> {

    SupplierInvoice findByDeliveryNumber(String dn);

    Page<SupplierInvoice> findBySupplierOrderPurchaseOrderNumber(String poNum, Pageable pageable);

    Set<SupplierInvoice> findByCurrentShipmentDestinationStation(Station station);

    Set<SupplierInvoice> findByCurrentShipment(Shipment shipment);


    @Query("select s.deliveryNumber from SupplierInvoice s where s.created between ?1 and ?2")
    List<String> findDeliveryNumberByCreatedBetween(Date startDate, Date endDate);

}
