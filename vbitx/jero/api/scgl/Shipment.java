/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.scgl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author gigadot
 */
@Entity
@Table(uniqueConstraints
        = @UniqueConstraint(columnNames = {"invoice_id", "legId"}),
        indexes = {
                @Index(columnList = "created", name = "created_idx"),
                @Index(columnList = "completed", name = "completed_idx"),
                @Index(columnList = "destination_id", name = "destination_station_idx"),
                @Index(columnList = "created,completed", name = "created_completed_idx")
        })
@EntityListeners(AuditingEntityListener.class)
public class Shipment implements Serializable, Comparable<Shipment> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private long id;

    private String legId;

    // completed flag flipped when doc return is completed
    private boolean completed = false;

    // has left origin, not yet arrived destination
    private boolean inTransit = false;

    // set after the shipment has arrived destination
    private Double averageTruckSpeed = null;

    private Double utilization = null;

    private String truckType = null;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified;

    @ManyToOne
    @JoinColumn(name = "issued_shipment_id")
    private IssuedShipment issuedShipment;

    @ManyToOne
    @JoinColumn(name = "invoice_id")
    private SupplierInvoice supplierInvoice;

    @ManyToOne
    @JoinColumn(name = "origin_id")
    private Station originStation;

    @ManyToOne
    @JoinColumn(name = "destination_id")
    private Station destinationStation;

    @ManyToOne
    @JoinColumn(name = "truck_id")
    private Truck truck;

    @ManyToOne
    @JoinColumn(name = "carrier_id")
    private Carrier carrier;

    @ManyToOne
    @JoinColumn(name = "driver_id")
    private Driver driver;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="actual",column=@Column(name="open_actual")),
            @AttributeOverride(name="plan",column=@Column(name="open_plan")),
            @AttributeOverride(name="dataSource",column=@Column(name="open_data_source")),
            @AttributeOverride(name="lastModified",column=@Column(name="open_last_modified")),
            })
    private ShipmentActivity open;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="actual",column=@Column(name="tender_actual")),
            @AttributeOverride(name="plan",column=@Column(name="tender_plan")),
            @AttributeOverride(name="dataSource",column=@Column(name="tender_data_source")),
            @AttributeOverride(name="lastModified",column=@Column(name="tender_last_modified")),
    })
    private ShipmentActivity tender;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="actual",column=@Column(name="ta_actual")),
            @AttributeOverride(name="plan",column=@Column(name="ta_plan")),
            @AttributeOverride(name="dataSource",column=@Column(name="ta_data_source")),
            @AttributeOverride(name="lastModified",column=@Column(name="ta_last_modified")),
    })
    private ShipmentActivity tenderAccept;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="actual",column=@Column(name="inbo_actual")),
            @AttributeOverride(name="plan",column=@Column(name="inbo_plan")),
            @AttributeOverride(name="dataSource",column=@Column(name="inbo_data_source")),
            @AttributeOverride(name="lastModified",column=@Column(name="inbo_last_modified")),
    })
    private ShipmentActivity inboundOrigin;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="actual",column=@Column(name="gr_actual")),
            @AttributeOverride(name="plan",column=@Column(name="gr_plan")),
            @AttributeOverride(name="dataSource",column=@Column(name="gr_data_source")),
            @AttributeOverride(name="lastModified",column=@Column(name="gr_last_modified")),
    })
    private ShipmentActivity gr;


    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="actual",column=@Column(name="gi_actual")),
            @AttributeOverride(name="plan",column=@Column(name="gi_plan")),
            @AttributeOverride(name="dataSource",column=@Column(name="gi_data_source")),
            @AttributeOverride(name="lastModified",column=@Column(name="gi_last_modified")),
    })
    private ShipmentActivity gi;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="actual",column=@Column(name="outbo_actual")),
            @AttributeOverride(name="plan",column=@Column(name="outbo_plan")),
            @AttributeOverride(name="dataSource",column=@Column(name="outbo_data_source")),
            @AttributeOverride(name="lastModified",column=@Column(name="outbo_last_modified")),
    })
    private ShipmentActivity outboundOrigin;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="actual",column=@Column(name="inbd_actual")),
            @AttributeOverride(name="plan",column=@Column(name="inbd_plan")),
            @AttributeOverride(name="dataSource",column=@Column(name="inbd_data_source")),
            @AttributeOverride(name="lastModified",column=@Column(name="inbd_last_modified")),
    })
    private ShipmentActivity inboundDestination;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="actual",column=@Column(name="outbd_actual")),
            @AttributeOverride(name="plan",column=@Column(name="outbd_plan")),
            @AttributeOverride(name="dataSource",column=@Column(name="outbd_data_source")),
            @AttributeOverride(name="lastModified",column=@Column(name="outbd_last_modified")),
    })
    private ShipmentActivity outboundDestination;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="actual",column=@Column(name="delivery_actual")),
            @AttributeOverride(name="plan",column=@Column(name="delivery_plan")),
            @AttributeOverride(name="dataSource",column=@Column(name="delivery_data_source")),
            @AttributeOverride(name="lastModified",column=@Column(name="delivery_last_modified")),
    })
    private ShipmentActivity delivery;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="actual",column=@Column(name="docret_actual")),
            @AttributeOverride(name="plan",column=@Column(name="docret_plan")),
            @AttributeOverride(name="dataSource",column=@Column(name="docret_data_source")),
            @AttributeOverride(name="lastModified",column=@Column(name="docret_last_modified")),
    })
    private ShipmentActivity docReturn;

    @Transient
    public static final String SINGULAR = Shipment.class.getSimpleName().toLowerCase();

    @Transient
    public static final String PLURAL = SINGULAR + "s";

    protected Shipment() {
        // empty
    }

    public Shipment(SupplierInvoice invoice, String legId) {
        this.supplierInvoice = invoice;
        this.legId = legId;
    }

    public long getId() {
        return id;
    }

    @Transient
    public String getDeliveryNumber() {
        return getSupplierInvoice().getDeliveryNumber();
    }

    public String getLegId() {
        return legId;
    }

    @JsonIgnore
    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @JsonIgnore
    public IssuedShipment getIssuedShipment() {
        return issuedShipment;
    }

    public void setIssuedShipment(IssuedShipment issuedShipment) {
        this.issuedShipment = issuedShipment;
    }

    @JsonIgnore
    public SupplierInvoice getSupplierInvoice() {
        return supplierInvoice;
    }

    public void setSupplierInvoice(SupplierInvoice supplierInvoice) {
        this.supplierInvoice = supplierInvoice;
    }

    public Station getOriginStation() {
        return originStation;
    }

    public void setOriginStation(Station originStation) {
        this.originStation = originStation;
    }

    public Station getDestinationStation() {
        return destinationStation;
    }

    public void setDestinationStation(Station destinationStation) {
        this.destinationStation = destinationStation;
    }

    public Truck getTruck() {
        return truck;
    }

    public void setTruck(Truck truck) {
        this.truck = truck;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public Carrier getCarrier() {
        return carrier;
    }

    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    public Double getAverageTruckSpeed() {
        return averageTruckSpeed;
    }

    public void setAverageTruckSpeed(Double averageTruckSpeed) {
        this.averageTruckSpeed = averageTruckSpeed;
    }

    public Double getUtilization() {
        return utilization;
    }

    public void setUtilization(Double utilization) {
        this.utilization = utilization;
    }

    @JsonIgnore
    public ShipmentActivity getOpen() {
        return open;
    }

    public void setOpen(ShipmentActivity open) {
        this.open = open;
    }

    @JsonIgnore
    public ShipmentActivity getTender() {
        return tender;
    }

    public void setTender(ShipmentActivity tender) {
        this.tender = tender;
    }

    @JsonIgnore
    public ShipmentActivity getTenderAccept() {
        return tenderAccept;
    }

    public void setTenderAccept(ShipmentActivity tenderAccept) {
        this.tenderAccept = tenderAccept;
    }

    @JsonIgnore
    public ShipmentActivity getInboundOrigin() {
        return inboundOrigin;
    }

    public void setInboundOrigin(ShipmentActivity inboundOrigin) {
        this.inboundOrigin = inboundOrigin;
    }

    @JsonIgnore
    public ShipmentActivity getGi() {
        return gi;
    }

    public void setGi(ShipmentActivity gi) {
        this.gi = gi;
    }

    @JsonIgnore
    public ShipmentActivity getOutboundOrigin() {
        return outboundOrigin;
    }

    public void setOutboundOrigin(ShipmentActivity outboundOrigin) {
        this.outboundOrigin = outboundOrigin;
    }

    @JsonIgnore
    public ShipmentActivity getInboundDestination() {
        return inboundDestination;
    }

    public void setInboundDestination(ShipmentActivity inboundDestination) {
        this.inboundDestination = inboundDestination;
    }

    @JsonIgnore
    public ShipmentActivity getGr() {
        return gr;
    }

    public void setGr(ShipmentActivity gr) {
        this.gr = gr;
    }

    @JsonIgnore
    public ShipmentActivity getOutboundDestination() {
        return outboundDestination;
    }

    public void setOutboundDestination(ShipmentActivity outboundDestination) {
        this.outboundDestination = outboundDestination;
    }

    @JsonIgnore
    public ShipmentActivity getDelivery() {
        return delivery;
    }

    public void setDelivery(ShipmentActivity delivery) {
        this.delivery = delivery;
    }

    @JsonIgnore
    public ShipmentActivity getDocReturn() {
        return docReturn;
    }

    public void setDocReturn(ShipmentActivity docReturn) {
        this.docReturn = docReturn;
    }

    @Transient
    @Override
    public int compareTo(Shipment o) {
        if (this.getDeliveryNumber().equals(o.getDeliveryNumber())) {
            return this.getLegId().compareTo(o.getLegId());
        } else {
            return this.getDeliveryNumber().compareTo(o.getDeliveryNumber());
        }
    }

    @Transient
    @JsonIgnore
    public Map asMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", getId());
        map.put("dn", getDeliveryNumber());
        map.put("legId", getLegId());
        map.put("created", getCreated());
        map.put("lastModified", getLastModified());
        map.put("completed", isCompleted());
        map.put("averageTruckSpeed", getAverageTruckSpeed());
        map.put("utilization", getUtilization());
        map.put("truckType", getTruckType());
        map.put("carrier", getCarrier() == null ? null : getCarrier().asMap());
        map.put("driver", getDriver() == null ? null : getDriver().asMap());
        map.put("originStation", getOriginStation() == null ? null : getOriginStation().asMap());
        map.put("destinationStation", getDestinationStation() == null ? null : getDestinationStation().asMap());
        map.put("truck", getTruck() == null ? null : getTruck().asMap());
        map.put("issuedShipment", getIssuedShipment() == null ? null : getIssuedShipment().asMap());
        return map;
    }

    @JsonIgnore
    @Transient
    public Map<String, ShipmentActivity> getShipmentActivities() {
        Map<String, ShipmentActivity> shipmentActivities = new HashMap<>();
        shipmentActivities.put("OPEN", getOpen());
        shipmentActivities.put("TENDER", getTender());
        shipmentActivities.put("TENDER_ACCEPT", getTenderAccept());
        shipmentActivities.put("INBOUND_ORIGIN", getInboundOrigin());
        shipmentActivities.put("GR", getGr());
        shipmentActivities.put("GI", getGi());
        shipmentActivities.put("OUTBOUND_ORIGIN", getOutboundOrigin());
        shipmentActivities.put("INBOUND_DESTINATION", getInboundDestination());
        shipmentActivities.put("OUTBOUND_DESTINATION", getOutboundDestination());
        shipmentActivities.put("DELIVERY", getDelivery());
        shipmentActivities.put("DOC_RETURN", getDocReturn());
        return shipmentActivities;
    }

    public boolean isInTransit() {
        return inTransit;
    }

    public void setInTransit(boolean inTransit) {
        this.inTransit = inTransit;
    }

    public String getTruckType() {
        return truckType;
    }

    public void setTruckType(String truckType) {
        this.truckType = truckType;
    }
}
