/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.api.scgl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * @author gigadot
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
public class SupplierInvoice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(nullable = false)
    private String deliveryNumber;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Enumerated(value = EnumType.STRING)
    private Status status = Status.Unknown;

    private int maxLegNumber;

    private int currentLegNumber;

    @ManyToOne
    @JoinColumn(name = "shipment_id")
    private Shipment currentShipment;

    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private SupplierOrder supplierOrder;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "supplierInvoice")
    @OrderBy("legId")
    private SortedSet<Shipment> shipments = new TreeSet<>();

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified;

    @Transient
    public static final String SINGULAR = StringUtils.uncapitalize(SupplierInvoice.class.getSimpleName());

    @Transient
    public static final String PLURAL = SINGULAR + "s";

    protected SupplierInvoice() {
    }

    public SupplierInvoice(String deliveryNumber, SupplierOrder supplierOrder) {
        this.deliveryNumber = deliveryNumber;
        this.supplierOrder = supplierOrder;
    }

    public SupplierOrder getSupplierOrder() {
        return supplierOrder;
    }

    public void setSupplierOrder(SupplierOrder supplierOrder) {
        this.supplierOrder = supplierOrder;
    }

    public SortedSet<Shipment> getShipments() {
        return shipments;
    }

    public void setShipments(SortedSet<Shipment> shipments) {
        this.shipments = shipments;
    }

    public String getDeliveryNumber() {
        return deliveryNumber;
    }

    public void setDeliveryNumber(String deliveryNumber) {
        this.deliveryNumber = deliveryNumber;
    }

    @JsonIgnore
    public Shipment getCurrentShipment() {
        return currentShipment;
    }

    public void setCurrentShipment(Shipment currentShipment) {
        this.currentShipment = currentShipment;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getStatus() {
        return status.name();
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getMaxLegNumber() {
        return maxLegNumber;
    }

    public void setMaxLegNumber(int maxLegNumber) {
        this.maxLegNumber = maxLegNumber;
    }

    public int getCurrentLegNumber() {
        return currentLegNumber;
    }

    public void setCurrentLegNumber(int currentLegNumber) {
        this.currentLegNumber = currentLegNumber;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Transient
    @JsonIgnore
    public Map asMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("deliveryNumber", getDeliveryNumber());
        map.put("lastModified", getLastModified());
        map.put("status", getStatus());
        map.put("supplierOrder", getSupplierOrder().asMap());
        map.put("maxLegNumber", getMaxLegNumber());
        map.put("currentLegNumber", getCurrentLegNumber());
        return map;
    }

    public enum Status {
        Unknown,
        Ongoing,
        Completed
    }
}
