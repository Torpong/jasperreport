/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp;

import org.hibernate.cfg.ImprovedNamingStrategy;

/**
 *
 * @author gigadot
 */
public class JeroNamingStrategy extends ImprovedNamingStrategy {

    private static final String PREFIX = "tbl_";

    @Override
    public String classToTableName(String className) {
        return addPrefix(super.classToTableName(className));
    }

    @Override
    public String collectionTableName(String ownerEntity, String ownerEntityTable, String associatedEntity,
            String associatedEntityTable, String propertyName) {
        return addPrefix(super.collectionTableName(ownerEntity,
                ownerEntityTable, associatedEntity, associatedEntityTable,
                propertyName));
    }

    @Override
    public String logicalCollectionTableName(String tableName, String ownerEntityTable,
            String associatedEntityTable, String propertyName) {
        return addPrefix(super.logicalCollectionTableName(tableName,
                ownerEntityTable, associatedEntityTable, propertyName));
    }

    private String addPrefix(String composedTableName) {
        return PREFIX + composedTableName;

    }
}
