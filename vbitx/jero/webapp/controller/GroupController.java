/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.group.Group;
import com.vbitx.jero.api.group.GroupRepository;
import com.vbitx.jero.api.group.GroupService;
import com.vbitx.jero.api.user.User;
import com.vbitx.jero.api.user.UserRepository;
import com.vbitx.jero.api.user.UserService;
import java.security.Principal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author sun
 */
@RestController
@RequestMapping(value = {"/groups"})
public class GroupController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private UserRepository userRepo;
    
    @Autowired
    private UserService userService;

    @Autowired
    private GroupRepository groupRepository;

    @RequestMapping(value = "/profile", method = {RequestMethod.GET})
    public Map getAllProfilePermissions(Principal principal) {
        String username = principal.getName();
        User user = userRepo.findByUsername(username);

        Map result = groupService.getGroupAndPermissionsByUser(user);
        return result;
    }
    
    @RequestMapping(value = "/{id}/permissions", method = {RequestMethod.POST})
    public Map togglePermissionToGroup(@PathVariable long id, String  name){
        boolean isExisted = groupService.togglePermissionToGroup(id, name);
        
        Map<String, Boolean> result = new HashMap();
        
        result.put("success", true);
        result.put(name, isExisted);
        return result;
    }

    @RequestMapping(value = {"/", ""}, method = {RequestMethod.GET})
    public Map getAllGroup() {
        Map<String, Iterable> result = new HashMap();
        result.put("groups", groupService.getAllGroups());
        return result;
    }
    
    @RequestMapping(value = {"/{id}"}, method = {RequestMethod.GET})
    public Map getGroupAndPermissions(@PathVariable long id) {
        return groupService.getGroupAndPermissions(id);
    }
    
    @RequestMapping(value = {"/{id}/info"}, method = {RequestMethod.GET})
    public Map getGroup(@PathVariable long id) {
        Map<String, Group> result = new HashMap();
        result.put("group", groupService.getGroup(id));
        return result;
    }
    
    @RequestMapping(value = {"/{id}/users"}, method = {RequestMethod.GET})
    public Map getGroupUsers(@PathVariable long id) {
        Map<String, Set> result = new HashMap();
        result.put("users", groupService.getGroupUsers(id));
        return result;
    }
    
    @RequestMapping(value = {"/{id}/users"}, method = {RequestMethod.POST})
    public Map addUserToGroup(@PathVariable long id, long userId) {
        Map<String, Boolean> result = new HashMap();
        User user =  userService.getUser(userId);
        boolean success = groupService.addUserToGroup(id, user);
        result.put("success", success);
        return result;
    }
    
    @RequestMapping(value = {"/{id}/users/{userId}/delete"}, method = {RequestMethod.POST})
    public Map removeUserToGroup(@PathVariable long id, @PathVariable long userId) {
        Map<String, Boolean> result = new HashMap();
        User user =  userService.getUser(userId);
        boolean success = groupService.removeUserFromGroup(id, user);
        result.put("success", success);
        return result;
    }
    
    @RequestMapping(value = {"/"}, method = {RequestMethod.POST})
    public Map createGroup(String name, String description) {
        Map<String, Group> result = new HashMap();
        Group grp = groupService.createGroup(name, description);
        result.put("group", grp);
        return result;
    }
    
    @RequestMapping(value = {"/{id}/delete"}, method = {RequestMethod.POST})
    public Map removeGroup(@PathVariable long id) {
        Map<String, Boolean> result = new HashMap();
        groupService.removeGroup(id);
        result.put("success", true);
        return result;
    }
    
    @RequestMapping(value = {"/{id}"}, method = {RequestMethod.POST})
    public Map updateGroup(@PathVariable long id, String name, String description) {
        Map<String, Group> result = new HashMap();
        result.put("group", groupService.updateGroup(id, name, description));
        return result;
    }
    

}
