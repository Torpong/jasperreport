package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.jasper.*;
import com.vbitx.jero.api.scgl.ShipmentActivity;
import com.vbitx.jero.api.scgl.service.DataService;
import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = {"/dev"})
public class DevController {

    private static final Logger logger = LoggerFactory.getLogger(DevController.class);

    @Autowired
    private DataService dataService;

    @Autowired
    private TaskExecutor executor;

    @Autowired
    private InvoiceDocumentService invoiceDocumentService;

    @Autowired
    private ShipmentDocumentService shipmentDocumentService;

    @Autowired
    private ShipmentWithAmountDocumentService shipmentWithAmountDocumentService;

    @Autowired
    private PayInSlipDocumentService payinslip;

    @Autowired
    private PickingListDocumentService pickinglist;

    @Autowired
    private ReceiptDocumentService receiptDocumentService;

//    @RequestMapping(value = {"/shipments"}, method = RequestMethod.POST)
//    public ResponseEntity receiveShipment(String shipments) {
//        // deprecated
//        return null;
//    }
//
//    @RequestMapping(value = {"/upload_shipments"}, method = {RequestMethod.POST}, consumes = {"*/*", "multipart/form-data"})
//    public ResponseEntity handleFileUpload(@RequestParam("shipments") MultipartFile file, RedirectAttributes redirectAttributes) {
//        Map<String, Object> responseMap = new HashMap<>();
//        boolean success = false;
//        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
//        String msg = "";
//        if (!file.isEmpty()) {
//            // save to disk if possible
//            try {
//                File outFile = new File(Long.toString(new Date().getTime()) + ".csv");;
//                FileUtils.touch(outFile);
//                FileUtils.writeByteArrayToFile(outFile, file.getBytes());
//            } catch (IOException ex) {
//                logger.warn("Error backing up CSV file.");
//            }
//            // Try auto-detect charset
//            Charset charset = StandardCharsets.UTF_8;
//            try {
//                charset = new AutoDetectReader(file.getInputStream()).getCharset();
//            } catch (TikaException e) {
//                // failed to auto-detect charset
//                // fall back to utf-8
//            } catch (IOException e) {
//                // read error
//                logger.warn(e.getMessage());
//                responseMap.put("success", false);
//                responseMap.put("message", e.getMessage());
//                return new ResponseEntity<>(responseMap, HttpStatus.BAD_REQUEST);
//            }
//            // try reading csv
//            MappingIterator<Map<String, String>> it;
//            try {
//                InputStreamReader streamReader = new InputStreamReader(file.getInputStream(), charset);
//                CsvMapper mapper = new CsvMapper();
//                CsvSchema schema = CsvSchema.emptySchema().withHeader();
//                it = mapper.readerFor(Map.class)
//                        .with(schema)
//                        .readValues(streamReader);
//            }  catch (IOException e) {
//                // read error
//                logger.warn(e.getMessage());
//                responseMap.put("success", false);
//                responseMap.put("message", e.getMessage());
//                return new ResponseEntity<>(responseMap, HttpStatus.BAD_REQUEST);
//            }
//
//            // Create a map keyed by DN
//            Map<String, List<Map<String, String>>> data = new HashMap<>();
//            while (it.hasNext()) {
//                try {
//                    Map<String, String> row = it.next();
//
//                    String DN = row.get(ConstantService.CsvFieldType.DELIVERY_NUMBER.getJsonString());
//                    if (DN != null && DN.length() > 0) {
//                        if (data.get(DN) == null) {
//                            List<Map<String, String>> rows = new ArrayList<>();
//                            rows.add(row);
//                            data.put(DN, rows);
//                        } else {
//                            data.get(DN).add(row);
//                        }
//                    }
//                } catch (Exception e) {
//                    // row error
//                    logger.warn(e.getMessage());
//                }
//            }
//            executor.execute(new UpdateShipmentTask(data));
//            success = true;
//            msg = "New data upload has been queued.";
//            httpStatus = HttpStatus.OK;
//        }
//        responseMap.put("success", success);
//        responseMap.put("message", msg);
//        return new ResponseEntity<>(responseMap, httpStatus);
//    }

    // All Parameter are String type
    @RequestMapping(value = {"/InvoicePDF"}, method = RequestMethod.GET)
    public ResponseEntity<byte[]> genPdfInvoice() {
        try{
            // InvoiceDoc class :
            // String transportationID, String transportationDate, String customerRetrieveDate, String invoiceNo, Integer total

            ArrayList<InvoiceDocumentService.InvoiceDocumentItemData> itemData = new ArrayList<>();
            itemData.add(new InvoiceDocumentService.InvoiceDocumentItemData("DP-C1160402108", "4/2/2016", "4/5/2016", "239018/300640/301910", 400.00));
            itemData.add(new InvoiceDocumentService.InvoiceDocumentItemData("DP-C1160402108", "4/2/2016", "4/5/2016", "239018/300640/301910", 400.00));
            itemData.add(new InvoiceDocumentService.InvoiceDocumentItemData("DP-C1160402108", "4/2/2016", "4/5/2016", "239018/300640/301910", 400.00));
            itemData.add(new InvoiceDocumentService.InvoiceDocumentItemData("DP-C1160402108", "4/2/2016", "4/5/2016", "239018/300640/301910", 400.00));


            //Parameter:
            //Seller, SellerAddress, Buyer, BuyerAddress, Branch, TaxID, DocumentDate,
            //DocumentID, TranspotationPeriod, PaymentDueDate, PaymentDueDate, StringInt

            InvoiceDocumentService.InvoiceDocumentData docData = new InvoiceDocumentService.InvoiceDocumentData(
                    "บริษัท เอลซีจี โลจิสติกส์แมเนจเม้นท์ จำกัด",
                    "เลขที่ 1 ถใปูนซิเมนต์ไทย บางชื่อ กรุงเทพ 10800 โทร. 02-8898752ม089-9244520 Fax.02-8898787",
                    "พุทธมณฑลสาย 4"
                    ,"0105533060315"
                    ,"บริษัท กัปตัน โค๊ทติ้ง จำกัด",
                    "906 หมู่ 15 ต.บางเสาธง อ.บางเสาธง",
                    "4/20/2016",
                    "BI - C1007/2016",
                    "1/4/2016 - 15/4/2016",
                    "5/5/2016"
                    ,"สามพันสองร้อยสิบห้้าบาทถ้วน",
                    itemData);


            ByteArrayOutputStream outputStream = invoiceDocumentService.generatePdf(docData);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            ResponseEntity<byte[]> response = new ResponseEntity<>(outputStream.toByteArray(), headers, HttpStatus.OK);
            return response;
        }
        catch (JRException e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @RequestMapping(value = {"/ShipmentPDF"}, method = RequestMethod.GET)
    public ResponseEntity<byte[]> genPdfShipment() {
        try{
            //ShipmentDoc class:
            // String DPNO, String DPDate, String sender, String getter, String remarkT

            ArrayList<ShipmentDocumentService.ShipmentDocumentItemData> itemData = new ArrayList<>();
            itemData.add(new ShipmentDocumentService.ShipmentDocumentItemData("DP-C1160402108", "4/2/2016", "เบเยอะร์ จำกัด", "ซีอาร์ซี เพาเวอร์รีเทล", ""));
            itemData.add(new ShipmentDocumentService.ShipmentDocumentItemData("DP-C1160402108", "4/2/2016", "เบเยอะร์ จำกัด", "ซีอาร์ซี เพาเวอร์รีเทล", "370622430"));
            itemData.add(new ShipmentDocumentService.ShipmentDocumentItemData("DP-C1160402108", "4/2/2016", "เบเยอะร์ จำกัด", "ซีอาร์ซี เพาเวอร์รีเทล", ""));


            ShipmentDocumentService.ShipmentDocumentData docData = new ShipmentDocumentService.ShipmentDocumentData(
                    "ภาคภูมิ โลจิสติกส์",
                    "70-3668",
                    "xxxxxxxxxxx",
                    "สายสี่",
                    "คลองหลวง",
                    "SM-C1160500375",
                    "12/05/2016",
                    itemData);

            ByteArrayOutputStream outputStream = shipmentDocumentService.generatePdf(docData);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            ResponseEntity<byte[]> response = new ResponseEntity<>(outputStream.toByteArray(), headers, HttpStatus.OK);
            return response;
        }
        catch (JRException e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = {"/ShipmentWithAmountPDF"}, method = RequestMethod.GET)
    public ResponseEntity<byte[]> genPdfShipmentWithAmount() {
        try{

            //ShipmentDoc2 class:
            // String DPNO, String paymed, String sender, String getter, String remarkT, Double total

            ArrayList<ShipmentWithAmountDocumentService.ShipmentWithAmountDocumentItemData> itemData = new ArrayList<>();
            itemData.add(new ShipmentWithAmountDocumentService.ShipmentWithAmountDocumentItemData("DP-C1160402108", "4/2/2016", "เบเยอะร์ จำกัด", "ซีอาร์ซี เพาเวอร์รีเทล", "", 0.00));
            itemData.add(new ShipmentWithAmountDocumentService.ShipmentWithAmountDocumentItemData("DP-C1160402108", "4/2/2016", "เบเยอะร์ จำกัด", "ซีอาร์ซี เพาเวอร์รีเทล", "370622430", 320.00));
            itemData.add(new ShipmentWithAmountDocumentService.ShipmentWithAmountDocumentItemData("DP-C1160402108", "4/2/2016", "เบเยอะร์ จำกัด", "ซีอาร์ซี เพาเวอร์รีเทล", "", 230.00));
            itemData.add(new ShipmentWithAmountDocumentService.ShipmentWithAmountDocumentItemData("DP-C1160402108", "4/2/2016", "เบเยอะร์ จำกัด", "ซีอาร์ซี เพาเวอร์รีเทล", "", 1000.00));
            Map parameters = new HashMap();

            //Parameter:
            //Contacter, Registration, Remark, HubDis, ShipmentNo, CreateDate, DocDate

            ShipmentWithAmountDocumentService.ShipmentWithAmountDocumentData docData = new ShipmentWithAmountDocumentService.ShipmentWithAmountDocumentData(
                    "ABC ภาคภูมิ โลจิสติกส์"
                    ,"ภจ-8373", "xxxxxxxxxxx"
                    , "ขอนแก่ง", "SM-C1160500375"
                    , "18/05/2016"
                    , itemData
            );


            ByteArrayOutputStream outputStream = shipmentWithAmountDocumentService.generatePdf(docData);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            ResponseEntity<byte[]> response = new ResponseEntity<>(outputStream.toByteArray(), headers, HttpStatus.OK);
            return response;
        }
        catch (JRException e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = {"/PayinPDF"}, method = RequestMethod.GET)
    public ResponseEntity<byte[]> genPdfPayInSlip() {
        try{
            //PayInSlip class:
            //String DPNO, String paymed, String paydate, String getter, String customer, double transferFee, double recrieveFee

            ArrayList<PayInSlipDocumentService.PayInSlipDocumentItemData> itemData = new ArrayList<>();
            itemData.add(new PayInSlipDocumentService.PayInSlipDocumentItemData("DP-C1160402108", "4/2/2016","เบเยอะร์ จำกัด","ซีอาร์ซี เพาเวอร์รีเทล","",0.00,100.00));
            itemData.add(new PayInSlipDocumentService.PayInSlipDocumentItemData("DP-C1160402108", "4/2/2016", "เบเยอะร์ จำกัด", "ซีอาร์ซี เพาเวอร์รีเทล", "370622430", 320.00, 100.00));
            itemData.add(new PayInSlipDocumentService.PayInSlipDocumentItemData("DP-C1160402108", "4/2/2016", "เบเยอะร์ จำกัด", "ซีอาร์ซี เพาเวอร์รีเทล", "", 230.00, 100.00));
            Map parameters = new HashMap();

            //Parameter:
            //hub, TransferNo, CreateDate
            PayInSlipDocumentService.PayInSlipDocumentData docData = new PayInSlipDocumentService.PayInSlipDocumentData(
                    "ขอนแก่ง", "SM-C1160500375", "18/05/2016", itemData
            );

            ByteArrayOutputStream outputStream = payinslip.generatePdf(docData);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            ResponseEntity<byte[]> response = new ResponseEntity<>(outputStream.toByteArray(), headers, HttpStatus.OK);
            return response;
        }
        catch (JRException e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @RequestMapping(value = {"/PickingListPDF"}, method = RequestMethod.GET)
    public ResponseEntity<byte[]> genPdfPickingList() {
        try{

            // PickingList Class:
            // String sender, String getter, String DPNO, List<subtableData> data
            // -----------Remark Last Element of PickingList take list of subtableData class------------
            // subtableData: Class:
            // String remark, String unit, String product, String productType, Integer number

            ArrayList<PickingListDocumentService.PickingListDocumentItemData> itemData = new ArrayList<>();

            List<PickingListDocumentService.subtableData> listItemsFirst = new ArrayList<>();
            PickingListDocumentService.subtableData dataFirst = new PickingListDocumentService.subtableData("NONE","BOX","Toy","plastic",2);
            PickingListDocumentService.subtableData dataSecond = new PickingListDocumentService.subtableData("NONE","BOX","PS4","console",3);

            listItemsFirst.add(dataFirst);
            listItemsFirst.add(dataSecond);

            itemData.add(new PickingListDocumentService.PickingListDocumentItemData("Bossy", "TaeWon", "123", listItemsFirst));
            itemData.add(new PickingListDocumentService.PickingListDocumentItemData("TaeWon", "John", "456", listItemsFirst));

            // -------------------------------------------------------------------------------------------

            List<PickingListDocumentService.subtableData> listItemsSecond = new ArrayList<>();
            PickingListDocumentService.subtableData dataTwoFirst = new PickingListDocumentService.subtableData("NONE","BOX","snack","unhealty",4);
            PickingListDocumentService.subtableData dataTwoSecond = new PickingListDocumentService.subtableData("NONE","BOX","Food","healty",5);

            listItemsSecond.add(dataTwoFirst);
//            listItemsSecond.add(dataTwoSecond);

            itemData.add(new PickingListDocumentService.PickingListDocumentItemData("P.Gen", "P.O", "789", listItemsSecond));
            itemData.add(new PickingListDocumentService.PickingListDocumentItemData("P.O", "Aj.Sunsurn", "111213", listItemsSecond));

            PickingListDocumentService.PickingListDocumentData docData = new PickingListDocumentService.PickingListDocumentData(
                    "ต่อพงศ์ จันทร์ตรี",
                    "SM-C1160500375", "ไม่มีอะไร",
                    "บางกรวย",
                    "Bangkok",
                    "12345648",
                    "18/05/2016",
                    itemData
            );

            ByteArrayOutputStream outputStream = pickinglist.generatePdf(docData);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            ResponseEntity<byte[]> response = new ResponseEntity<>(outputStream.toByteArray(), headers, HttpStatus.OK);
            return response;
        }
        catch (JRException e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @RequestMapping(value = {"/ReceiptPDF"}, method = RequestMethod.GET)
    public ResponseEntity<byte[]> genPdfReceipt() {
        try{
            // InvoiceDoc class :
            // String transferFee, Double total

            ArrayList<ReceiptDocumentService.ReceiptDocumentItemData> itemData = new ArrayList<>();
            itemData.add(new ReceiptDocumentService.ReceiptDocumentItemData("DP-C1160402108", 400.00));
            itemData.add(new ReceiptDocumentService.ReceiptDocumentItemData("DP-C1160402108", 400.00));
            itemData.add(new ReceiptDocumentService.ReceiptDocumentItemData("DP-C1160402108", 400.00));
            itemData.add(new ReceiptDocumentService.ReceiptDocumentItemData("DP-C1160402108", 400.00));


            //Parameter:
            //Seller, SellerAddress, Buyer, BuyerAddress, Branch, BuyerTaxID, DocumentDate,
            //,SellerTaxID, NoNum, RefNo, StringInt

            ReceiptDocumentService.ReceiptDocumentData docData = new ReceiptDocumentService.ReceiptDocumentData(
                    "บริษัท เอลซีจี โลจิสติกส์แมเนจเม้นท์ จำกัด",
                    "เลขที่ 1 ถใปูนซิเมนต์ไทย บางชื่อ กรุงเทพ 10800 โทร. 02-8898752ม089-9244520 Fax.02-8898787",
                    "พุทธมณฑลสาย 4", "IP88842SF23", "บริษัท กัปตัน โค๊ทติ้ง จำกัด",
                    "906 หมู่ 15 ต.บางเสาธง อ.บางเสาธง",
                    "0105533060315","4/20/2016",
                    "ET54988456",
                    "879854232",
                    "สามพันสองร้อยสิบห้้าบาทถ้วน",
                    itemData
            );


            ByteArrayOutputStream outputStream = receiptDocumentService.generatePdf(docData);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            ResponseEntity<byte[]> response = new ResponseEntity<>(outputStream.toByteArray(), headers, HttpStatus.OK);
            return response;
        }
        catch (JRException e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }


    private class UpdateShipmentTask implements Runnable {
        
        Map<String, List<Map<String, String>>> data;
        
        public UpdateShipmentTask(Map<String, List<Map<String, String>>> data) {
            this.data = data;
        }
        
        @Override
        public void run() {
            for (String dn : data.keySet()) {
                dataService.processDN(dn, data, ShipmentActivity.DataSource.SCGL_FEED);
            }
            dataService.rebuildDailyCache(data);
        }
    }
}
