package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.scgl.Shipment;
import com.vbitx.jero.api.scgl.ShipmentActivity;
import com.vbitx.jero.api.scgl.SupplierInvoice;
import com.vbitx.jero.api.scgl.service.ShipmentService;
import com.vbitx.jero.api.scgl.service.SupplierInvoiceService;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by sun on 6/13/2016 AD.
 */
@RestController
@RequestMapping(value = "/supplier_invoices")
public class SupplierInvoiceController {

    private static Logger logger = LoggerFactory.getLogger(SupplierInvoiceController.class);

    @Autowired
    private SupplierInvoiceService supplierInvoiceService;

    @Autowired
    private ShipmentService shipmentService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public Map getAll(Integer page, Integer limit) {
        Map<String, Object> responseMap = new HashMap<>();
        if (page == null || limit == null) {
            responseMap.put("success", false);
            responseMap.put("message", "cannot find page and limit");
            return responseMap;
        }
        responseMap.put(SupplierInvoice.PLURAL, supplierInvoiceService.listAll(page, limit));
        return responseMap;
    }

    @RequestMapping(value = {"/{dn}"}, method = RequestMethod.GET)
    public Map getById(@PathVariable String dn) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(SupplierInvoice.SINGULAR, supplierInvoiceService.findByDeliveryNumber(dn));
        return responseMap;
    }

    @RequestMapping(value = {"/search_by_po_number/{poNum}"}, method = RequestMethod.GET)
    public Map listByPoNumber(@PathVariable String poNum, Integer page, Integer limit) {
        Map<String, Object> responseMap = new HashMap<>();
        if (page == null || limit == null) {
            responseMap.put("success", false);
            responseMap.put("message", "cannot find page and limit");
            return responseMap;
        }

        responseMap.put(SupplierInvoice.PLURAL, supplierInvoiceService.listByPurchaseNumber(poNum, page, limit));
        return responseMap;
    }

    // TODO: Refactor this controller
    @RequestMapping(value = {"/dn/{dn}"}, method = RequestMethod.GET)
    public Map getByDN(@PathVariable String dn) {
        Map<String, Object> responseMap = new HashMap<>();
        SupplierInvoice si = supplierInvoiceService.findByDeliveryNumber(dn);
        if (si != null) {
            List<Shipment> shipments = shipmentService.searchByDeliveryNumber(dn);

            List<StationInformation> stations = new ArrayList<>();
            for (int i = 0; i < shipments.size() + 1; i++) {
                stations.add(new StationInformation());
            }

            List<ShipmentActivityWrapper> shipmentActivities = new ArrayList<>();
            for (int i = 0; i < shipments.size(); i++) {
                Shipment shipment = shipments.get(i);
                stations.get(i).name = shipment.getOriginStation().getName();
                stations.get(i).inbound |= (shipment.getInboundOrigin().getActual() != null);

                ShipmentActivity sa = shipment.getOutboundOrigin();
                if (sa.getActual() != null) {
                    shipmentActivities.add(0, new ShipmentActivityWrapper(
                            "OUTBOUND_ORIGIN",
                            sa.getActual(),
                            shipment.getOriginStation().getName()));
                }
                stations.get(i).outbound = (sa.getActual() != null);
                // already outbound, marked as inbound as well
                stations.get(i).inbound |= stations.get(i).outbound;

                sa = shipment.getInboundDestination();
                if (sa.getActual() != null) {
                    shipmentActivities.add(0, new ShipmentActivityWrapper(
                            "INBOUND_DESTINATION",
                            sa.getActual(),
                            shipment.getDestinationStation().getName()));
                }
                stations.get(i + 1).name = shipment.getDestinationStation().getName();
                stations.get(i + 1).inbound |= (sa.getActual() != null);
            }

            responseMap.put("stations", stations);
            if (shipments.size() > 0) {
                Shipment lastShipment = shipments.get(shipments.size() - 1);
                responseMap.put("delivered", lastShipment.getDelivery().getActual() != null);
                responseMap.put("delivery_time", lastShipment.getDelivery().getActual());
            } else {
                responseMap.put("delivered", false);
                responseMap.put("delivery_time", null);
            }
            responseMap.put(SupplierInvoice.SINGULAR, supplierInvoiceService.findByDeliveryNumber(dn));
            responseMap.put("shipmentactivities", shipmentActivities);
        }
        return responseMap;
    }

    @RequestMapping(value = {"/process_monitoring/search_by_dn/{dn}"}, method = RequestMethod.GET)
    public Map getAllProcessesByDn(@PathVariable String dn) {
        Map<String, Object> responseMap = new HashMap<>();
        List<Object> wrapper = new ArrayList<>();
        wrapper.add(supplierInvoiceService.findByProcessMonitoringDN(dn));
        responseMap.put("allProcesses", wrapper);
        return responseMap;
    }

    @RequestMapping(value = {"/process_monitoring/list_all"}, method = RequestMethod.GET)
    public Map getAllProcessesByDate() {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("allProcesses", supplierInvoiceService.listAllProcessMonitoring());
        return responseMap;
    }

    @RequestMapping(value = {"/process_monitoring/list_by_date/{date}"}, method = RequestMethod.GET)
    public Map getAllProcessesByDate(@PathVariable String date) {
        Map<String, Object> responseMap = new HashMap<>();
        Date sDate = new Date(Long.parseLong(date));
        responseMap.put("allProcesses", supplierInvoiceService.listProcessMonitoringByDate(sDate));
        return responseMap;
    }

    @RequestMapping(value = {"/process_monitoring/list"}, method = RequestMethod.GET)
    public Map getAllProcessesByDateParam(
            @RequestParam(value = "date", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
            @RequestParam(value = "minify", required = false, defaultValue = "false") boolean minify) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("success", true);
        if (date != null) {
            responseMap.put("allProcesses", supplierInvoiceService.listProcessMonitoringByDate(date));
        } else {
            responseMap.put("allProcesses", new ArrayList<Object>());
        }
        return responseMap;
    }

    private static final Map<String, String> keyMap = new HashMap<>();

    static {
        keyMap.put("PLAN_OUTBOUND_ORIGIN", "POO");
        keyMap.put("PLAN_INBOUND_ORIGIN", "PIO");
        keyMap.put("OUTBOUND_ORIGIN", "OO");
        keyMap.put("INBOUND_ORIGIN", "IO");
    }

    private Map minify(Map<String, Object> map) {
        Map<String, Object> minMap = new HashMap<>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            Object newValue = null;
            if (value instanceof Map) {
                newValue = minify((Map<String, Object>) value);
            } else if (value instanceof List) {
                List list = (List) value;
                List newList = new ArrayList();
                // based on assumption that there is no immediate nested list
                for (Object object : list) {
                    if (object instanceof Map) {
                        newList.add(minify((Map<String, Object>) object));
                    } else {
                        newList.add(object);
                    }
                }
                newValue = newList;
            } else {
                newValue = value;
            }
            String newKey = keyMap.get(key);
            minMap.put((newKey != null) ? newKey : key, newValue);
        }
        return minMap;
    }

    private class StationInformation {

        public String name;
        public boolean inbound;
        public boolean outbound;

        public StationInformation() {
            this.inbound = false;
            this.outbound = false;
        }
    }

    private class ShipmentActivityWrapper {

        public String name;
        public Date timestamp;
        public String stationName;

        public ShipmentActivityWrapper(String name, Date timestamp, String stationName) {
            this.name = name;
            this.timestamp = timestamp;
            this.stationName = stationName;
        }

    }
}
