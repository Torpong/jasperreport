package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.scgl.SupplierOrder;
import com.vbitx.jero.api.scgl.service.SupplierOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sun on 6/13/2016 AD.
 */
@RestController
@RequestMapping(value = "/supplier_orders")
public class SupplierOrderController {

    @Autowired
    private SupplierOrderService supplierOrderService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public Map getAll() {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(SupplierOrder.PLURAL, supplierOrderService.listAll());
        return responseMap;
    }

    @RequestMapping(value = {"/{orderNumber}"}, method = RequestMethod.GET)
    public Map getById(@PathVariable String orderNumber) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(SupplierOrder.SINGULAR, supplierOrderService.findByOrderNumber(orderNumber));
        return responseMap;
    }
}
