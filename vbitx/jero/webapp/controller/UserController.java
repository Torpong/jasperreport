package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.user.credential.UserCredential;
import com.vbitx.jero.api.user.credential.UserCredentialService;
import com.vbitx.jero.api.user.profile.UserProfile;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.vbitx.jero.api.user.UserRepository;
import com.vbitx.jero.api.authority.AuthorityRepository;
import com.vbitx.jero.api.group.GroupService;
import com.vbitx.jero.api.user.User;
import com.vbitx.jero.api.user.UserService;
import com.vbitx.jero.api.user.authority.UserAuthorityRepository;
import com.vbitx.jero.api.user.credential.UserCredentialRepository;
import com.vbitx.jero.api.user.profile.UserProfileRepository;
import com.vbitx.jero.api.user.exception.UserNotFoundException;
import com.vbitx.jero.api.user.profile.UserProfileService;
import com.vbitx.jero.core.exception.converter.ExceptionToMessageConverter;
import com.vbitx.jero.module.user.manangement.UserManagementService;
import java.util.Set;

@RestController
@RequestMapping(value = {"/users"})
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private UserProfileService userProfileService;

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private UserManagementService userManagementService;

    @Autowired
    private UserAuthorityRepository userAuthorityRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private UserCredentialService userCredentialService;

    @Autowired
    private UserCredentialRepository userCredentialRepository;

    @Autowired
    private ExceptionToMessageConverter exceptionToMessageConverter;

    @RequestMapping(value = {"/", ""}, method = {RequestMethod.GET})
    public Map listUsers(Principal principal) {
        List<User> users = IteratorUtils.toList(userRepository.findAll().iterator());

        Map<String, List<User>> msg = new HashMap<>();
        msg.put("users", users);
        return msg;
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    public Map getUser(@PathVariable long id) {
        UserProfile uprofile = userProfileService.getUserProfileById(id);
        Set<String> permissions = groupService.getUserPermissions(uprofile.getUser());

        Map<String, Object> msg = new HashMap<>();
        msg.put("user", uprofile);
        msg.put("permissions", permissions);

        return msg;
    }

    @RequestMapping(value = "/me/profile", method = {RequestMethod.GET})
    public Map myProfile(Principal principal) {
        String username = principal.getName();
        UserProfile uprofile = userProfileService.getUserProfileByUsername(username);
        Set<String> permissions = groupService.getUserPermissions(uprofile.getUser());

        Map<String, Object> msg = new HashMap<>();
        msg.put("user", uprofile);
        msg.put("permissions", permissions);
        return msg;
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.POST)
    @Transactional
    public Map updateUser(Principal principal, String firstname, String lastname, @PathVariable long id) {
        User user = userRepository.findOne(id);
        UserProfile userProfile = userProfileRepository.findByUserId(id);
        userProfile.setFirstname(firstname);
        userProfile.setLastname(lastname);
        userRepository.save(user);
        userProfileRepository.save(userProfile);

        Map<String, Object> msg = new HashMap<>();
        msg.put("user", userProfile);
        return msg;

    }

    @RequestMapping(value = {"/{id}/deactivate"}, method = RequestMethod.POST)
    public Map deactivateUser(@PathVariable long id) {
        try {
            Map<String, Object> msg = new HashMap<>();
            userService.setUserDisabledStatus(id, false);
            msg.put("success", true);
            return msg;
        } catch (UserNotFoundException ex) {
            // can change UserNotFoundException to IndicativeException to catch all
            return exceptionToMessageConverter.convert(ex);
        }
    }

    @RequestMapping(value = {"/{id}/change_password"}, method = RequestMethod.POST)
    @Transactional
    public Map changePassword(@PathVariable long id, String password, String cpassword) {
        Map<String, Object> msg = new HashMap<>();
        if (!password.equals(cpassword) || password.length() < 6) {
            msg.put("success", false);
            msg.put("message", "Confirmed password does not match or password is too short (>= 6 characters).");
            return msg;
        }
        User user = userRepository.findOne(id);
        String salt = userCredentialService.generateSalt();
        UserCredential userCredential = userCredentialRepository.findByUser(user);
        userCredential.setSalt(salt);
        userCredential.setHashedPassword(userCredentialService.encryptPassword(password, salt));
        userCredentialRepository.save(userCredential);

        msg.put("success", true);
        return msg;
    }

    @RequestMapping(value = {"/", ""}, method = RequestMethod.POST)
    public Map createUser(String username, String email, String password, String firstname, String lastname) {
        Map<String, Object> msg = new HashMap<>();
        msg.put("success", userManagementService.createUser(username, email, password, firstname, lastname));
        return msg;

    }

//    @RequestMapping(value = "/logout", method = {RequestMethod.GET, RequestMethod.POST})
//    public Map logout(HttpServletRequest request, HttpServletResponse response) {
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        if (auth != null) {
//            new SecurityContextLogoutHandler().logout(request, response, auth);
//        }
//        Map<String, String> msg = new HashMap<>();
//        msg.put("message", "You have successfully logged out.");
//        return msg;
//    }
}
