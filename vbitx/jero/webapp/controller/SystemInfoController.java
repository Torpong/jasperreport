package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.core.sysinfo.SystemInfo;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wp214
 */
@RestController
@RequestMapping("/sysinfo")
public class SystemInfoController {
    
    public final String VERSION = "1.0";
    
    @Autowired
    private SystemInfo systemInfo;

    @RequestMapping(method = RequestMethod.GET, value = {"/", ""})
    public Map info(HttpServletRequest request) {
        Map sysinfo = systemInfo.get(request);
        sysinfo.put("version", VERSION);
        return sysinfo;
    }

}
