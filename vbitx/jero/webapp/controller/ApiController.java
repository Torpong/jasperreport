/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp.controller;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.vbitx.jero.api.scgl.ShipmentActivity;
import com.vbitx.jero.api.scgl.service.ApiKeyService;
import com.vbitx.jero.api.scgl.service.ConstantService;
import com.vbitx.jero.api.scgl.service.DataService;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.tika.detect.AutoDetectReader;
import org.apache.tika.exception.TikaException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author gigadot
 */
@RestController
@RequestMapping(value = "/api")
public class ApiController {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    private ApiKeyService apiKeyService;

    @Autowired
    private DataService dataService;

    @Autowired
    private TaskExecutor executor;

    @RequestMapping(value = {"/upload_csv"}, method = {RequestMethod.POST}, consumes = {"*/*", "multipart/form-data"})
    public ResponseEntity handleFileUpload(@RequestHeader(value = "APIKEY") String key,
            @RequestParam("data") MultipartFile file) {
        if (apiKeyService.isValid(key)) {
            if (!file.isEmpty()) {
                // save to disk if possible
                try {
                    File outFile = new File(Long.toString(new Date().getTime()) + ".csv");;
                    FileUtils.touch(outFile);
                    FileUtils.writeByteArrayToFile(outFile, file.getBytes());
                } catch (IOException ex) {
                    logger.warn("Error backing up CSV file.");
                }
                // Try auto-detect charset
                Charset charset = StandardCharsets.UTF_8;
                try {
                    charset = new AutoDetectReader(file.getInputStream()).getCharset();
                } catch (TikaException e) {
                    // failed to auto-detect charset
                    // fall back to utf-8
                } catch (IOException e) {
                    // read error
                    logger.warn(e.getMessage());
                    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }
                // try reading csv
                MappingIterator<Map<String, String>> it;
                try {
                    InputStreamReader streamReader = new InputStreamReader(file.getInputStream(), charset);
                    CsvMapper mapper = new CsvMapper();
                    CsvSchema schema = CsvSchema.emptySchema().withHeader();
                    it = mapper.readerFor(Map.class)
                            .with(schema)
                            .readValues(streamReader);
                }  catch (IOException e) {
                    // read error
                    logger.warn(e.getMessage());
                    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }

                // Create a map keyed by DN
                Map<String, List<Map<String, String>>> data = new HashMap<>();
                while (it.hasNext()) {
                    try {
                        Map<String, String> row = it.next();
                        String DN = row.get(ConstantService.CsvFieldType.DELIVERY_NUMBER.getJsonString());
                        if (DN != null && DN.length() > 0) {
                            if (data.get(DN) == null) {
                                List<Map<String, String>> rows = new ArrayList<>();
                                rows.add(row);
                                data.put(DN, rows);
                            } else {
                                data.get(DN).add(row);
                            }
                        }
                    } catch (Exception e) {
                        // row error
                        logger.warn(e.getMessage());
                    }
                }
                executor.execute(new UpdateShipmentTask(data));
            }
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    private class UpdateShipmentTask implements Runnable {

        Map<String, List<Map<String, String>>> data;

        public UpdateShipmentTask(Map<String, List<Map<String, String>>> data) {
            this.data = data;
        }

        @Override
        public void run() {
            for (String dn : data.keySet()) {
                dataService.processDN(dn, data, ShipmentActivity.DataSource.SCGL_FEED);
            }
            dataService.rebuildDailyCache(data);
        }
    }
}
