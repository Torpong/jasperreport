package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.scgl.Shipment;
import com.vbitx.jero.api.scgl.Station;
import com.vbitx.jero.api.scgl.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sun on 6/13/2016 AD.
 */
@RestController
@RequestMapping(value = "/stations")
public class StationController {

    @Autowired
    private StationService stationService;

    @Autowired
    private XdockService xdockService;

    @Autowired
    private IssuedShipmentService issuedShipmentService;

    @Autowired
    private SupplierInvoiceService supplierInvoiceService;


    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public Map getAllStations() {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(Station.PLURAL, stationService.listAll());
        return responseMap;
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public Map getStation(@PathVariable long id) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(Station.SINGULAR, stationService.getById(id));
        return responseMap;
    }

    @RequestMapping(value = {"/", ""}, method = RequestMethod.POST, consumes = {"application/json"})
    public Map createStation(@RequestBody Station station){
        Station newStation = stationService.create(station.getName());
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(Station.SINGULAR, newStation);
        return responseMap;
    }

    @RequestMapping(value = {"/{id}/inbound_shipments"}, method = RequestMethod.GET)
    public Map listInboundShipment(@PathVariable int id, Integer page, Integer limit, Principal principal) {
        // list shipments that about to inbound
        Map<String, Object> responseMap = new HashMap<>();
        // do permission check
//        if (!stationService.canAccess(principal.getName(),id)) return responseMap;
        // get all shipments not yet received
        responseMap.put(Shipment.PLURAL, xdockService.getInboundShipmentsByStationId(id, page, limit));
        return responseMap;
    }

    @RequestMapping(value = {"/inbound_shipments/{shipmentId}"}, method = RequestMethod.GET)
    public Map inboundShipmentDetail(@PathVariable String shipmentId, Principal principal) {
        Map<String, Object> responseMap = new HashMap<>();
        // do permission check
        // if (!stationService.canAccess(principal.getName(),id)) return responseMap;
        responseMap.put("issuedShipmentDetail", issuedShipmentService.getIssuedShipmentDetail(shipmentId));
        return responseMap;
    }

    @RequestMapping(value = {"/outbound_shipments/{shipmentId}"}, method = RequestMethod.GET)
    public Map outboundShipmentDetail(@PathVariable long shipmentId, Principal principal) {
        Map<String, Object> responseMap = new HashMap<>();
        // do permission check
        // if (!stationService.canAccess(principal.getName(),id)) return responseMap;
        responseMap.put("issuedShipmentDetail", xdockService.getXdockShipment(shipmentId));
        return responseMap;
    }

    @RequestMapping(value = {"/{id}/supplier_invoices"}, method = RequestMethod.GET)
    public Map listInHubSupplierInvoice(@PathVariable long id) {
        // list inhub shipment
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(Shipment.PLURAL, xdockService.listAvailableXdockInvoices(id));
        return responseMap;
    }

    @RequestMapping(value = {"/{id}/outbound_shipments"}, method = RequestMethod.GET)
    public Map listOutboundShipment(@PathVariable int id, Integer page, Integer limit) {
        // list of outbound shipment
        Map<String, Object> responseMap = new HashMap<>();

        responseMap.put(Shipment.PLURAL, xdockService.getUnissuedXdockShipment(id, page, limit));
        return responseMap;
    }

    @RequestMapping(value = {"/{id}/receive_shipment"}, method = RequestMethod.POST)
    public Map receiveShipmentToHubById(@PathVariable long id, String shippingNumber) {
        // should receive shipment
        Map<String, Object> responseMap = new HashMap<>();
        Date date = new Date();
        xdockService.receiveShipment(id, shippingNumber, date);
        responseMap.put("success", true);
        return responseMap;
    }

    @RequestMapping(value = {"/{id}/shipments"}, method = RequestMethod.POST)
    public Map createShipmentInHub(@PathVariable long id, String driverName, String truckLicense) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("shipment", xdockService.createXdockShipment(id, driverName, truckLicense));
        return responseMap;
    }

    @RequestMapping(value = {"/shipments/{id}/update"}, method = RequestMethod.POST)
    public Map updateXdockShipment(@PathVariable long id, String driverName, String truckLicense) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("shipment", xdockService.updateXdockShipment(id, driverName, truckLicense));
        return responseMap;
    }

    @RequestMapping(value = {"/shipments/{id}/add_invoice"}, method = RequestMethod.POST)
    public Map addXdockInvoiceToShipment(@PathVariable long id, String deliveryNumber) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("shipment", xdockService.addXdockInvoiceToShipment(deliveryNumber, id));
        return responseMap;
    }

    @RequestMapping(value = {"/shipments/{id}/remove_invoice"}, method = RequestMethod.POST)
    public Map removeXdockInvoiceFromShipment(@PathVariable long id, String deliveryNumber) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("shipment", xdockService.removeXdockInvoiceFromShipment(deliveryNumber, id));
        return responseMap;
    }

    @RequestMapping(value = {"/{id}/available_invoices"}, method = RequestMethod.GET)
    public Map listAvailableXdockInvoices(@PathVariable long id) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("supplier_invoices", xdockService.listAvailableXdockInvoices(id));
        return responseMap;
    }

    @RequestMapping(value = {"shipments/{id}/issue"}, method = RequestMethod.POST)
    public Map issueShipment(@PathVariable long id) {
        Map<String, Object> responseMap = new HashMap<>();
        Date date = new Date();
        xdockService.issueShipment(id, date);
        responseMap.put("success", true);
        return responseMap;
    }

}
