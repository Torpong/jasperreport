package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.scgl.Customer;
import com.vbitx.jero.api.scgl.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sun on 6/13/2016 AD.
 */
@RestController
@RequestMapping(value = "/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public ResponseEntity getCustomers() {
        List<Customer> customers = customerService.listAll();
        Map<String, List<Customer>> responseMap = new HashMap<>();
        responseMap.put("customers", customers);
        return new ResponseEntity<>(responseMap, HttpStatus.OK);
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public ResponseEntity getCustomer(@PathVariable long id) {
        Customer customer = customerService.findById(id);
        Map<String, Customer> responseMap = new HashMap<>();
        responseMap.put("customer", customer);
        return new ResponseEntity<>(responseMap, HttpStatus.OK);
    }
}
