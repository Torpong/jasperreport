package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.scgl.ShipmentActivityMaster;
import com.vbitx.jero.api.scgl.service.ShipmentActivityMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sun on 6/13/2016 AD.
 */
@RestController
@RequestMapping(value = "/shipment_status_plans")
public class ShipmentActivityMasterController {

    @Autowired
    private ShipmentActivityMasterService shipmentActivityMasterService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public Map getAllMaster() {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("shipmentActivityMaster", shipmentActivityMasterService.listAll());
        return responseMap;
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public Map getStation(@PathVariable long id) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("shipmentActivityMaster", shipmentActivityMasterService.getById(id));
        return responseMap;
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.POST)
    public Map update(@PathVariable long id, long plan) {
//        Date date = null;
//        try {
//            SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a");
//            date = formatter.parse(plan);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        Date date = new Date(plan);
        ShipmentActivityMaster shipmentActivityMaster =
                shipmentActivityMasterService.update(id, date);
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("shipmentActivityMaster", shipmentActivityMaster);
        return responseMap;
    }
}
