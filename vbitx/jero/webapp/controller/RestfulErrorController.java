/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.core.exception.converter.ExceptionToMessageConverter;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

@RestController
public class RestfulErrorController implements ErrorController {

    private static final String PATH = "/error";

    @Autowired
    private ErrorAttributes errorAttributes;
    
    @Autowired
    private ExceptionToMessageConverter exceptionToMessageConverter;

    @RequestMapping(value = {"/Access_Denied", PATH})
    public Map error(HttpServletRequest request) {
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);

        Throwable throwable = errorAttributes.getError(requestAttributes);
        Map<String, Object> errorAttributesMap = errorAttributes.getErrorAttributes(requestAttributes, true);
        
        return exceptionToMessageConverter.convert(throwable, errorAttributesMap);
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
