package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.scgl.IssuedShipment;
import com.vbitx.jero.api.scgl.Shipment;
import com.vbitx.jero.api.scgl.repository.ShipmentRepository;
import com.vbitx.jero.api.scgl.service.IssuedShipmentService;
import com.vbitx.jero.api.scgl.service.ShipmentService;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by sun on 6/13/2016 AD.
 */
@RestController
@RequestMapping(value = "/shipments")
public class ShipmentController {

    @Autowired
    private ShipmentService shipmentService;

    @Autowired
    private IssuedShipmentService issuedShipmentService;


    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public Map getAllShipments(
            @RequestParam("from") @DateTimeFormat(pattern="yyy-MM-dd") Date from, 
            @RequestParam("to") @DateTimeFormat(pattern="yyy-MM-dd") Date to, 
            @RequestParam("status") String status, Integer page, Integer limit) {
        Map<String, Object> responseMap = new HashMap<>();
        if(page == null || limit == null){
            responseMap.put("success", false);
            responseMap.put("message", "cannot find page and limit");
        } else {
            to = DateUtils.addDays(to, 1);
            responseMap.put("success", true);
            responseMap.put(Shipment.PLURAL, issuedShipmentService.listScgShipment(page, limit, from, to, status));
        }
        return responseMap;
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public Map getById(@PathVariable String id) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(Shipment.SINGULAR, issuedShipmentService.getIssuedShipmentDetail(id));
        return responseMap;
    }


//    @RequestMapping(value = {"/{id}/supplier_invoices/{invoiceId}"}, method = RequestMethod.POST)
//    public Map addSupplierInvoiceToOutboundShipment(@PathVariable long id, @PathVariable long invoiceId) {
//        // should add supplier invoice to shipment
//        Map<String, Object> responseMap = new HashMap<>();
//        responseMap.put(Shipment.SINGULAR, shipmentService.getById(id));
//        return responseMap;
//    }



//    @RequestMapping(value = {"/{id}/outbound"}, method = RequestMethod.POST)
//    public Map listOutobundShipment(@PathVariable long id) {
//        // should list outbound shipment
//        Map<String, Object> responseMap = new HashMap<>();
//        //responseMap.put(Shipment.SINGULAR, shipmentService.listAll());
//        return responseMap;
//    }


}
