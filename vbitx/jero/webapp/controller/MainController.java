package com.vbitx.jero.webapp.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @RequestMapping("/")
    public Map index(Principal principal) {
        String username = "";
        if (principal != null) {
            username = principal.getName();
        }
        Map<String, String> msg = new HashMap<>();
        msg.put("message", "Welcome " + username + "!");
        return msg;
    }

    @RequestMapping(value = "/logout", method = {RequestMethod.GET, RequestMethod.POST})
    public Map logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        Map<String, String> msg = new HashMap<>();
        msg.put("message", "You have successfully logged out.");
        return msg;
    }

    @RequestMapping(value = "/login", method = {RequestMethod.GET})
    public Map login() {
        Map<String, String> msg = new HashMap<>();
        msg.put("message", "Method not allowed");
        return msg;
    }
    
    @RequestMapping(value = "/ping", method = {RequestMethod.GET})
    public Map checkAuthenticated() {
        Map<String, Object> msg = new HashMap<>();
        msg.put("success", true);
        return msg;
    }

}
