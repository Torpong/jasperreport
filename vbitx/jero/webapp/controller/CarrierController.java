package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.scgl.Carrier;
import com.vbitx.jero.api.scgl.service.CarrierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sun on 6/13/2016 AD.
 */
@RestController
@RequestMapping(value = "/carriers")
public class CarrierController {

    @Autowired
    private CarrierService carrierService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public Map getAll(Integer page, Integer limit) {
        Map<String, Object> responseMap = new HashMap<>();
        if(page == null || limit == null){
            responseMap.put("success", false);
            responseMap.put("message", "cannot find page and limit");
        }else{
            responseMap.put("success", true);
            responseMap.put(Carrier.PLURAL, carrierService.listAll(page, limit));
        }
        return responseMap;
    }

    @RequestMapping(value = {"/", ""}, method = RequestMethod.POST)
    public Map createStation(String code, String name) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(Carrier.SINGULAR, carrierService.create(code, name));
        return responseMap;

    }

    @RequestMapping(value = {"/{code}"}, method = RequestMethod.GET)
    public Map get(@PathVariable String code) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(Carrier.SINGULAR, carrierService.getByCode(code));
        return responseMap;
    }

}
