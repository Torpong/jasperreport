/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.ability.TotalPermissionService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author sun
 */
@RestController
@RequestMapping(value = {"/permissions"})
public class PermissionController {
    @Autowired
    private TotalPermissionService tps; 
    
    @RequestMapping(value =  {"/", ""}, method = {RequestMethod.GET})
    public Map getAllPermissions() {
        Map result = tps.getAllPermissions();
        return result;
    }
}
