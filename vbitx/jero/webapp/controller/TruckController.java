package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.scgl.Carrier;
import com.vbitx.jero.api.scgl.Truck;
import com.vbitx.jero.api.scgl.service.CarrierService;
import com.vbitx.jero.api.scgl.service.TruckService;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sun on 6/13/2016 AD.
 */
@RestController
@RequestMapping(value = "/trucks")
public class TruckController {

    @Autowired
    private TruckService truckService;

    @Autowired
    private CarrierService carrierService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public Map getAllTrucks() {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(Truck.PLURAL, truckService.listAll());
        return responseMap;
    }

    @RequestMapping(value = {"/", ""}, method = RequestMethod.POST)
    public Map createTruck(String license, String carrierId) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(Truck.SINGULAR, truckService.create(license, carrierId));
        return responseMap;

    }

    @RequestMapping(value = {"/{license}"}, method = RequestMethod.GET)
    public Map getByLicense(@PathVariable String license) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(Truck.SINGULAR, truckService.getByLicense(license));
        return responseMap;
    }

    @RequestMapping(value = {"/{license}/gps_locations"}, method = RequestMethod.GET)
    public Map getLocationsByLicense(@PathVariable String license) {
        //Truck truck = truckService.getByLicense(license);
        Map<String, Object> responseMap = new HashMap<>();
        //responseMap.put(GpsLocation.PLURAL, gpsLocationService.listLocationsByTruck(truck));
        return responseMap;
    }
    
    @RequestMapping(value = {"/{license}/speed"}, method = RequestMethod.GET)
    public Map getCurrentSpeedByLicense(@PathVariable String license) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("speed", truckService.getCurrentSpeed(license));
        return responseMap;
    }



}
