package com.vbitx.jero.webapp.controller;

import com.vbitx.jero.api.scgl.Supplier;
import com.vbitx.jero.api.scgl.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sun on 6/13/2016 AD.
 */
@RestController
@RequestMapping(value = "/suppliers")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public Map getAllSupplier() {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(Supplier.PLURAL, supplierService.listAll());
        return responseMap;
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public Map getById(@PathVariable long id) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(Supplier.SINGULAR, supplierService.getById(id));
        return responseMap;
    }

}
