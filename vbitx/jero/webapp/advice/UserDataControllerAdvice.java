/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp.advice;

import com.vbitx.jero.api.user.profile.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import com.vbitx.jero.api.user.profile.UserProfileRepository;

/**
 *
 * @author sun
 */
@ControllerAdvice
public class UserDataControllerAdvice {

    @Autowired
    private UserProfileRepository userProfileRepository;

    @ModelAttribute
    public void addUserToModel(Model model) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            UserProfile userprofile = userProfileRepository.findByUserUsername(username);
            
            model.addAttribute("userProfile", userprofile);
        } catch (Exception ex) {

        }
    }

}
