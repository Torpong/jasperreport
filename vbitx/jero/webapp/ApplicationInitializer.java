/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vbitx.jero.webapp;

import com.vbitx.jero.api.authority.Authority;
import com.vbitx.jero.api.scgl.*;
import com.vbitx.jero.api.scgl.repository.*;
import com.vbitx.jero.api.scgl.service.*;
import com.vbitx.jero.api.user.User;
import com.vbitx.jero.api.user.authority.UserAuthority;
import com.vbitx.jero.api.user.credential.UserCredential;
import com.vbitx.jero.api.user.credential.UserCredentialService;
import com.vbitx.jero.api.user.profile.UserProfile;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import com.vbitx.jero.api.user.UserRepository;
import com.vbitx.jero.api.authority.AuthorityRepository;
import com.vbitx.jero.api.group.Group;
import com.vbitx.jero.api.group.GroupRepository;
import com.vbitx.jero.api.user.authority.UserAuthorityRepository;
import com.vbitx.jero.api.user.credential.UserCredentialRepository;
import com.vbitx.jero.api.user.profile.UserProfileRepository;

/**
 *
 * @author gigadot
 */
@Component
public class ApplicationInitializer {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserCredentialRepository userCredentialRepository;
    @Autowired
    private UserProfileRepository userProfileRepository;
    @Autowired
    private UserCredentialService userCredentialService;
    @Autowired
    private AuthorityRepository authorityRepository;
    @Autowired
    private UserAuthorityRepository userAuthorityRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private ApiKeyRepository apiKeyRepository;

    @Autowired
    private CustomerService customerService;
    @Autowired
    private CarrierService carrierService;
    @Autowired
    private GpsService gpsService;
    @Autowired
    private ShipmentService shipmentService;
    @Autowired
    private StationService stationService;
    @Autowired
    private SupplierInvoiceService supplierInvoiceService;
    @Autowired
    private SupplierOrderService supplierOrderService;
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private TruckService truckService;
    @Autowired
    private ShipmentActivityMasterService shipmentActivityMasterService;

    private void addMockNewUser(String username, String firstname, String lastname, String role) {
        User user = new User(username, username + "@vbitx.com");
        userRepository.save(user);

        UserCredential ucredential = new UserCredential();
        ucredential.setUser(user);
        String salt = userCredentialService.generateSalt();
        ucredential.setSalt(salt);
        ucredential.setHashedPassword(userCredentialService.encryptPassword("123456", salt));
        userCredentialRepository.save(ucredential);

        UserProfile uprofile = new UserProfile();
        uprofile.setFirstname(firstname);
        uprofile.setLastname(lastname);
        uprofile.setUser(user);
        userProfileRepository.save(uprofile);

        UserAuthority adminAuthority = new UserAuthority(user, authorityRepository.findByNameIgnoreCase(role));
        userAuthorityRepository.save(adminAuthority);
        UserAuthority userAuthority = new UserAuthority(user, authorityRepository.findByNameIgnoreCase("user"));
        userAuthorityRepository.save(userAuthority);
        
        Group grp = groupRepository.findByNameIgnoreCase("admin");
        grp.getUsers().add(user);
       

    }

    @EventListener
    @Transactional
    public void populateDatabase(ContextRefreshedEvent event) {
        if(groupRepository.findByNameIgnoreCase("admin") == null){
            Group grp = new Group();
            grp.setName("admin");
            grp.setDescription("This is for administrator only.");
            grp.getPermissions().add("main::sidebar");
            grp.getPermissions().add("main::admin");
            groupRepository.save(grp);
        }
        
        if(groupRepository.findByNameIgnoreCase("staff") == null){
            Group grp = new Group();
            grp.setName("staff");
            grp.setDescription("This is for staff only.");
            grp.getPermissions().add("main::sidebar");
            grp.getPermissions().add("main::staff");
            groupRepository.save(grp);
        }
        
        if(groupRepository.findByNameIgnoreCase("client") == null){
            Group grp = new Group();
            grp.setName("client");
            grp.setDescription("This is for client only.");
            grp.getPermissions().add("main::sidebar");
            grp.getPermissions().add("main::client");
            groupRepository.save(grp);
        }
        
        if (authorityRepository.findByNameIgnoreCase("admin") == null) {
            authorityRepository.save(new Authority("admin"));
        }

        if (authorityRepository.findByNameIgnoreCase("user") == null) {
            authorityRepository.save(new Authority("user"));
        }

        if (userRepository.findByUsername("admin") == null) {
            addMockNewUser("admin", "vBitx", "Admin", "admin");
        }

        if (userRepository.findByUsername("ucam") == null) {
            addMockNewUser("ucam", "vBitx", "ucam", "admin");
        }

        String mockKey = "81F14E895BE31E3958E7788ECC47E";
        if (apiKeyRepository.findBySecretKey(mockKey) == null) {
            ApiKey apikey = new ApiKey(mockKey, null);
            apiKeyRepository.save(apikey);
        }

        if(shipmentActivityMasterService.listAll().isEmpty()){
            for(ShipmentActivityMaster.Activity sae : ShipmentActivityMaster.Activity.values()){
                shipmentActivityMasterService.create(sae, null);
            }

        }

//        //Customer
//        if(customerService.listAll().isEmpty()){
//            customerService.create("customer name 1", "address 1");
//            customerService.create("customer name 2", "address 2");
//        }
//
//        // Supplier
//        if(supplierService.listAll().isEmpty()){
//            supplierService.create("Supplier 1", "address 1");
//            supplierService.create("Supplier 2", "address 2");
//        }
//
//        // Supplier Order
//        if(supplierOrderService.listAll().isEmpty()){
//            if(!supplierService.listAll().isEmpty() && !customerService.listAll().isEmpty()){
//                int i = 0;
//                for(Supplier supplier: supplierService.listAll()){
//                    for(Customer customer: customerService.listAll()){
//                        i++;
//                        supplierOrderService.create("number" + i, "purchase order no. " + i, supplier, customer);
//                    }
//                }
//            }
//            // Supplier Invoice
//            if(supplierInvoiceService.listAll().isEmpty()){
//                for(SupplierOrder supplierOrder: supplierOrderService.listAll()){
//                    supplierInvoiceService.create("SI1 -> " + supplierOrder.getNumber(), supplierOrder);
//                    supplierInvoiceService.create("SI2 -> " + supplierOrder.getNumber(), supplierOrder);
//                }
//            }
//        }
//
//        //Station
//        if(stationService.listAll().isEmpty()){
//            Station station1 = new Station("code1", "name1", "address1", "street1", "city1", "postcode1", "region code1", "region name1");
//            Station station2 = new Station("code2", "name2", "address1", "street2", "city2", "postcode2", "region code2", "region name2");
//            stationService.create(station1);
//            stationService.create(station2);
//        }

        //Shipment
//        if(shipmentService.listAll().isEmpty() && carrierService.listAll().isEmpty() && truckService.listAll().isEmpty()){
//            System.out.println("Shipment init -----");
//            carrierService.create("code1", "name 1");
//            carrierService.create("code2", "name 2");
//            System.out.println("Create carrier");
//            for(Carrier carrier: carrierService.listAll()){
//                System.out.println("Create truck");
//                truckService.create("truck1 -> " + carrier.getActivity(), carrier);
//                truckService.create("truck2 -> " + carrier.getActivity(), carrier);
//                for(Truck truck: truckService.listAll()){
//                    System.out.println("Create GPS");
//                    gpsLocationService.create("location1 ->" + truck.getLicense(), "lat", "long", truck);
//                    gpsLocationService.create("location2 ->" + truck.getLicense(), "lat", "long", truck);
//                }
//            }
//            List<Station> stations = stationService.listAll();
//            for(Truck truck: truckService.listAll()){
//                System.out.println("Create shipment");
//                Shipment sh1 = shipmentService.create(stations.get(0), stations.get(1), truck, "driver1 -> " + truck.getLicense(), "truck number1");
//                Shipment sh2 = shipmentService.create(stations.get(1), stations.get(0), truck, "driver2 -> " + truck.getLicense(), "truck number2");
//                for(SupplierInvoice supplierInvoice: supplierInvoiceService.listAll()){
//                    System.out.println("Add supplierInvoice to shipment");
//                    sh1.getSupplierInvoices().add(supplierInvoice);
//                    sh2.getSupplierInvoices().add(supplierInvoice);
//                }
//            }
//        }

    }

}
